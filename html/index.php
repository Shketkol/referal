
<!DOCTYPE html>
<head>
    <title>Образовательный проект «LIKE-БИЗНЕС» - Платежный кабинет</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Keywords" content="" />
    <meta name="Description" content="Образовательный проект «LIKE-БИЗНЕС» - Платежный кабинет" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/default.css" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="js/modernizr.js"></script> <!-- Modernizr -->
    <script src="js/modernizr.custom.js"></script>
    <script src="https://form.kupivkredit.ru/sdk/v1/sdk.js" type="text/javascript" async></script>
    <script type="text/javascript">

        function loadSDK(path, fnName) {
            var scriptPath, firstScript, scriptElement;

            scriptPath = path + "?onload=" + fnName;

            firstScript   = document.getElementsByTagName("script")[0];
            scriptElement = document.createElement("script");
            scriptElement.src = scriptPath;
            firstScript.parentNode.insertBefore(scriptElement, firstScript.nextSibling || firstScript);
        }

        loadSDK("https://form.kupivkredit.ru/sdk/v1/sdk.js", "myOnLoadFunction");

    </script>

    <style>
        body
        {
            background: #f1f0ef;
        }
        .displaynone
        {
            display:none;
        }

        .err b{
            border: #FF0000 2px solid;
        }
        .cd-form
        {
            position: relative;
            background: #fff;
            border-radius: 20px;
            padding: 20px 40px 20px 40px;
        }
        #itogo{
            margin: 0 0 65px 0; position: absolute; bottom: 0; display:none;
        }

        .container {
            width: 970px;
            margin: 0 auto;
            position: relative;
        }
        .header {
            height: 110px;
        }
        .logo {
            position: absolute;
            left: 0;
            top: 15px;
        }
        .contacts {
            position: absolute;
            right: 0;
            top: 38px;
            text-align: right;
        }
        .phone a{
            font-size: 26px;
            color: #6a4f32;
            font-weight: 200;
            text-decoration: none;
        }
        .email a{
            font-size: 22px;
            color: #6a4f32;
            font-weight: 200;
            text-decoration: none;
        }
        a.a-callback {
            text-decoration: none;
            border-bottom: 1px dashed #6a4f32;
            color: #6a4f32;
            font-size: 18px;
            margin-right: 3px;
            font-weight: 200;
        }
        .footer {
            height: 85px;
            margin: 20px 0 30px;
        }
        .foot-inform {
            color: #b9b9b9;
            font-size: 13px;
            text-align: left;
            text-transform: uppercase;
            line-height: 15px;
            position: absolute;
            left: 0px;
        }
        .foot-contacts {
            position: absolute;
            right: 0;
            top: 0;
            text-align: right;
        }
        .modal-window {
            width: 800px;
            height: auto;
            border-radius: 0;
            text-align: center;
            padding: 20px 0 30px;
        }

        .modal-header {
            text-align: center;
            color: #212121;
            font-weight: 200;
            font-size: 44px;
            display: block;
            padding: 30px 0 5px;
        }

        .modal-desc {
            text-align: center;
            color: #666666;
            font-weight: 200;
            font-size: 20px;
            display: block;
            padding: 0 0 10px;
        }

        .modal-info {
            color: #888888;
            font-size: 20px;
            padding: 15px 0 30px;
        }

        .modal-input {
            margin: 10px 0;
            border: 1px solid #d7d7d7;
            background: #ffffff;
            padding: 20px 25px;
            width: 260px;
            font-weight: 200;
            font-size: 16px;
        }

        .modal_success {
            font-size: 24px;
            text-align: center;
            padding: 20px 50px;
            font-weight: 200;
        }

        .tooltip {
            color: #000000; outline: none;
            cursor: help; text-decoration: none;
            position: relative;
        }
        .tooltip span {
            margin-left: -999em;
            position: absolute;
        }
        .tooltip:hover span {
            border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            font-family: Calibri, Tahoma, Geneva, sans-serif;
            position: absolute; left: 1em; top: 2em; z-index: 99;
            margin-left: 0; width: 250px;
        }

        .tooltip:hover em {
            font-family: Candara, Tahoma, Geneva, sans-serif; font-size: 1.2em; font-weight: bold;
            display: block; padding: 0.2em 0 0.6em 0;
        }
        .classic { padding: 0.8em 1em; }
        .custom { padding: 0.5em 0.8em 0.8em 2em; }

        .classic {background: #ebebeb; border: 0; text-align:left; }
        .bg-blue{
            background: #2c97de !important;
        }
    </style>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="logo">
            <img src="./img/logo.png" alt="" />
        </div>
        <div class="contacts">
            <div class="phone"><a href="tel:88003338701">8-800-333-87-01</a></div>
            <div class="email"><a href="mailto:info@likebz.ru">info@likebz.ru</a></div>
        </div>
    </div>
</div>

<div class="cd-form floating-labels">
    <fieldset>
        <h1 style="font-weight: 100; color: #878787; text-align: center; font-size: 28px;">Платежный кабинет Like Центр</h1>
        <div style='max-width:550px; min-width:250px; margin:0 auto;'>
            <div ><b style="font-weight: bold; font-size: 21px;">Вы оплачиваете:</b>
                <span style="font-size: 22px;">
Инсайдер</span>
            </div>
            <div><b style="font-weight: bold; font-size: 21px;">Всего к оплате:</b> <span style="font-size: 24px;">
1
руб.
</span></div>
<!--            название сервера-->
            <form id='payform' action='http://referal.new.local/payments/robokassa/index' method=POST>
                <input type="hidden" name="product">
                <input type="hidden" name="user">
                <input type="text" name="client_name" placeholder="Имя" required>
                <input type="email" name="client_email" placeholder="Email" required style="margin-top:10px">
                <input type="text" name="client_phone" placeholder="Телефон" required style="margin-top:10px; margin-bottom:10px">
                <input type=submit name=submit12 value=Оплатить style='background: #dd0031;' id='datavalid'>
            </form>

            <script type="text/javascript">

                window.callbacks = [];

                window.onload = function() {
                    for (var i = 0; i < this.callbacks.length; i++) {
                        this.callbacks[i].call();
                    }
                };

                window.myOnLoadFunction = function(KVK) {
                    var button, form;
                    form = KVK.ui("form", {
                        order:"eyJpdGVtcyI6W3sidGl0bGUiOiJcdTA0MThcdTA0M2RcdTA0NDFcdTA0MzBcdTA0MzlcdTA0MzRcdTA0MzVcdTA0NDAiLCJxdHkiOjEsInByaWNlIjoiMSJ9XSwicGFydG5lcklkIjoiYTA2YjAwMDAwMktENXBOQUFUIiwicGFydG5lck9yZGVySWQiOiJ0ZXN0X29yZGVyXzU5ZjU3YWM3NmE1YzUifQ==",
                        sign: "d3465f176a903752f0483b29b753e7e4",
                        type: "full"
                    });

                    window.callbacks.push(function() {
                        button = document.getElementById("open");
                        button.removeAttribute("disabled");
                        button.onclick = function() {
                            // Открытие формы по нажатию кнопки
                            form.open();
                        };
                    });
                }

            </script>
            <!-- <a style="display: inherit;
             text-align: center;
             border: none;
             width: 100%;
             margin-top: 2%;
             background: #3751b3;
             border-radius: .25em;
             padding: 16px 20px;
             color: #ffffff;
             font-weight: bold;
             cursor: pointer;
             -webkit-font-smoothing: antialiased;
             -moz-osx-font-smoothing: grayscale;
             -webkit-appearance: none;
             -moz-appearance: none;
             -ms-appearance: none;
             -o-appearance: none;
             appearance: none;" href="https://bill.qiwi.com/order/external/create.action?null&amp;from=464713&amp;summ=1&amp;currency=RUB&amp;to=" title="Далее">Оплатить QIWI</a>-->
            <input type="submit" id="open" name="open" value="Купи в кредит" style='    margin-top: 10px;'>
            <div style='width:100%; text-align:center; margin: 6px;'><a href='credit.pdf' target="_blanck" style='color: #000; font-size: 12px; text-transform: uppercase;'>Подробнее о Купи в Кредит</a></div>
        </div>
    </fieldset>
</div>

<div class="md-modal md-effect-16" id="modal-16">
    <div class="md-content">
        <div>
            <div id="error_text" style="text-align: center;  padding: 40px;"></div>
            <div><input type="text" id="inputemailvalid" style="display:none;     width: 250px;
    height: 30px;
    background: none;
    border: 2px solid #fff;
    text-align: center;
    color: #fff;
    margin: 0 auto 30px;
    font-size: 18px;" value="" maxlength="4" onkeypress="if(event.keyCode<48 || event.keyCode > 57)event.returnValue=false" ></div>
            <button class="md-close mdbutton" id="button_error"><div id="button_error_text"></div></button>
        </div>
    </div>
</div>

<div class="md-overlay"></div>
<div class="md-trigger" data-modal="modal-16"></div>


<div class="footer">
    <div class="container">
        <div class="foot-inform">

            ВСЕ ПРАВА ЗАЩИЩЕНЫ, 2016 <br>
            ООО «ЛАЙК БИЗНЕС» <br>
            ОГРН: 5157746032230 <br>
            123100, г.Москва, Пресненская набережная,<br/> д.12, Башня А, этаж 29<br>

            <a href="http://likebz.ru/oferta/oferta_form44.pdf" target="_blank">договор-оферты</a>
        </div>
        <div class="foot-contacts">
            <div class="phone"><a href="tel:88003338701">8-800-333-87-01</a></div>
            <div class="email"><a href="mailto:info@likebz.ru">info@likebz.ru</a></div>
        </div>
    </div>
</div>

<div style="text-align: center;">
    <div class="container">
        <a href="./paytypes/Webmoney/webmoney.htm" class="fancybox" data-fancybox-type="iframe"><img src="img/payt/image007.png"/></a>
        <a href="./paytypes/Yandex/yandex.htm" class="fancybox" data-fancybox-type="iframe"><img src="img/payt/image008.png"/></a>
        <a href="./paytypes/QIWI/qiwi.htm" class="fancybox" data-fancybox-type="iframe"><img src="img/payt/image009.png"/></a>
        <a href="./paytypes/BankCards/bankcards.htm" class="fancybox" data-fancybox-type="iframe"><img src="img/payt/image011.png"/></a>
        <a href="./paytypes/BankCards/bankcards.htm" class="fancybox" data-fancybox-type="iframe"><img src="img/payt/image012.png"/></a>
    </div>
</div>
<!--
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
window.criteo_q = window.criteo_q || [];
window.criteo_q.push(
{ event: "setAccount", account: 43290 },
{ event: "setEmail", email: "a.tavakai@likebz.ru" },
{ event: "setSiteType", type: "d" },
{ event: "trackTransaction", id: "43497732", item: [
{ id: "chat_proriv", price: 1, quantity: 1 }
/* добавьте строку для каждого товара в корзине пользователя */
]}
);
</script> -->


<script src="js/main.js"></script> <!-- Resource jQuery -->

<script>
    $('#payform').ready(function(){
        var url = window.location,
            search = url.search,
            href = url.href;
        var user = null;
        var req = /user=(\d+)/;
        user = search.match(req);

        if(user != null){
            $('input[name="user"]').val(user[1]);
        }

        var req = /&user=(\d+)/ig;
        var product = href.replace(req, '');
        if(product != null){
            $('input[name="product"]').val(product);
        }
    });
</script>

<script>

    $('#datavalid').on('click', function(){
        if (Number($('input[name="OutSum"]').val()) == 0) {
            $(this).addClass('bg-blue').val('Спасибо! Ваша заявка принята');
            return false;
        } ;
    });



    function price_up(){

        var price =  get_cookie('price_id');
        var dop_price_id =  get_cookie('dop_price_id');
        var data_type_pay =  get_cookie('data_type_pay');
        var dop_price2_id =  get_cookie('dop_price2_id');
        var num_place =  get_cookie('num_place');

        if(data_type_pay == 'no') price = 0;

        if(num_place == null) num_place = 1;
        if(dop_price_id == null) new_price = price;
        else var new_price = (parseFloat(price) + parseFloat(dop_price_id))*num_place;
        if(dop_price2_id !== null) new_price = parseFloat(new_price) + parseFloat(dop_price2_id);

        document.getElementById('price').innerHTML = '<b>'+new_price+' руб</b>';
        document.cookie = 'total_price='+new_price+';';
    }

    function delete_cookie ( cookie_name )
    {
        var cookie_date = new Date ( );  // Текущая дата и время
        cookie_date.setTime ( cookie_date.getTime() - 1 );
        document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
    }

    function $_GET(key) {
        var s = window.location.search;
        s = s.match(new RegExp(key + '=([^&=]+)'));
        return s ? s[1] : false;
    }

    function get_cookie ( cookie_name ) {
        var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
        if ( results )return ( unescape ( results[2] ) );
        else return null;
    }
    //var ddd = get_cookie('product_id');

    function send(id_select) {
        var price =$('.city-select_'+id_select).val();
        document.getElementById('price_'+id_select).innerHTML = '<b>'+price+' руб</b>';
        document.cookie = 'price_id='+price+';';
        price_up();
    }

    function send_product() { //выбор продукта
        var product_id_back =  get_cookie('product_id');
        var price = 0;
        if(product_id_back !== null) { //при выборе продукта скрытие предыдущего
            document.getElementById('cityid_'+product_id_back).style.display='none';
            document.getElementById('itogo').style.display='none';
        }
        document.cookie = 'price_id='+price+';';
        document.cookie =  'dop_price_id='+price+';';
        document.cookie = 'dop_price2_id='+price+';';
        document.cookie = 'data_type_pay=yes;';

        $("input[type=checkbox]").each(function() { this.checked=false; }); //сброс чекбоксов
        $("input[type=radio]").each(function() { this.checked=false; }); //сброс радио
        $('#cit_'+product_id_back+' option').prop('selected', function() { return this.defaultSelected;}); //сброс списка городов
        price_up();

        var product =$('.product-select').val();
        if(product == 'new_biz2' || product == 'concentrat' || product == 'scale_conf' || product == 'preaks' || product == 'army' || product == 'partner' || product == 'like_sochi' || product == 'aks' || product == 'dolina' || product == 'Business-Scaling' || product == 'army_year' || product == 'kons' || product == 'mentor' || product == 'army_net' || product == 'scaling_testdrive' || product == 'dolina_concentrat' || product == 'td_dolina' || product == 'sinking' || product == 'marketing_pay' || product == 'lf_kart' || product == 'fine' || product == 'chat_proriv' || product == 'pusk' || product == 'puskdolina' || product == 'puskdolinamas' || product == 'soсhi') {
            document.getElementById('cityid_'+product).style.display='block';
            document.getElementById('itogo').style.display='block';
            document.cookie = 'product_id='+product+';';
        }

        $('.rad_'+product+' input').change(function(){
            var id_rad = $(this).attr("id");
            var data_type_pay = $(this).attr("data-type-pay");
            document.cookie = 'data_type_pay='+data_type_pay+';';
            document.cookie = 'dop_price_id='+id_rad+';';
            price_up();

        });

        $('.check_'+product+' input').change(function(){
            var id_check = $(this).attr("id");
            var class_check = $(this).attr("class");
            var dop_price2_id =  get_cookie('dop_price2_id');

            if(this.checked){
                if(dop_price2_id == null) document.cookie = 'dop_price2_id='+id_check+';';
                else {
                    var new_dop_price2_id = parseFloat(dop_price2_id) + parseFloat(id_check);
                    document.cookie = 'dop_price2_id='+new_dop_price2_id+';';
                }
            }
            else{
                if(dop_price2_id == null) document.cookie = 'dop_price2_id='+id_check+';';
                else {
                    var new_dop_price2_id = parseFloat(dop_price2_id) - parseFloat(id_check);
                    document.cookie = 'dop_price2_id='+new_dop_price2_id+';';
                }
            }
            price_up();
        });
    }

    function send_product_active(product) { //открытие продукта при наличае GET параметра product
        document.getElementById('cityid_'+product).style.display='block';
        document.cookie = 'product_id='+product+';';
        var price = 0;
        document.cookie = 'price_id='+price+';';
        document.getElementById('price').innerHTML = '<b>'+price+'</b>';
        document.getElementById('itogo').style.display='block';
        price_up();



        $('.rad_'+product+' input').change(function(){
            var id_rad = $(this).attr("id");
            var data_type_pay = $(this).attr("data-type-pay");
            document.cookie = 'data_type_pay='+data_type_pay+';';
            document.cookie = 'dop_price_id='+id_rad+';';
            price_up();

        });

        $('.check_'+product+' input').change(function(){
            var id_check = $(this).attr("id");
            var class_check = $(this).attr("class");
            var dop_price2_id =  get_cookie('dop_price2_id');

            if(this.checked){
                if(dop_price2_id == null) document.cookie = 'dop_price2_id='+id_check+';';
                else {
                    var new_dop_price2_id = parseFloat(dop_price2_id) + parseFloat(id_check);
                    document.cookie = 'dop_price2_id='+new_dop_price2_id+';';
                }
            }
            else{
                if(dop_price2_id == null) document.cookie = 'dop_price2_id='+id_check+';';
                else {
                    var new_dop_price2_id = parseFloat(dop_price2_id) - parseFloat(id_check);
                    document.cookie = 'dop_price2_id='+new_dop_price2_id+';';
                }
            }
            price_up();
        });
    }
    var product = $_GET('product');
    if(product == false) product = 0;
    else send_product_active(product);

    $('.cit select').change(function(){ //выбор города
        var id_select = $(this).attr("id");
        send(id_select);
    });

    $('#product select').change(function(){ //выбор продукта
        delete_cookie ( "dop_price_id" );
        delete_cookie ( "dop_price2_id" );
        delete_cookie ( "num_place" );
        delete_cookie ( "data_type_pay" );
        send_product();
    });

    $('.place').change(function(){ //количество мест
        var id_place = $(this).attr("id");
        var num_place = $('#'+id_place).val();
        document.cookie = 'num_place='+num_place+';';
        price_up();
    });


    $('.valid').on("click", function(){	//валидация данных при отправке на второй этап

        var id_valid = $(this).attr("id");
        var data_type_pay = $(this).attr("data-type-pay");
        if(data_type_pay == 'sber') setTimeout( function() { location.replace("http://likebz.ru/pay/sber.php");}, 100);
        else {
            if($('input[name="'+id_valid+'"]:checked').length === 0) {
                document.getElementById('error_text').innerHTML = '<b>Выберите формат участия</b>';
                document.getElementById('button_error_text').innerHTML = 'Выбрать';
                document.getElementById('inputemailvalid').style.display='none';
                $('.md-trigger').trigger('click');
            }
            else {
                price =$('.city-select_cit_'+id_valid).val();/*
                 if(price == 0) {

                 document.getElementById('error_text').innerHTML = '<b>Выберите город</b>';
                 document.getElementById('button_error_text').innerHTML = 'Выбрать';
                 document.getElementById('inputemailvalid').style.display='none';
                 $('.md-trigger').trigger('click');

                 }
                 else
                 {
                 */
                place = $('#place_'+id_valid).val();
                if(place == 0 || place =='' || place ==' ')
                {
                    document.getElementById('error_text').innerHTML = '<b>Введите количество мест</b>';
                    document.getElementById('button_error_text').innerHTML = 'Ввести';
                    document.getElementById('inputemailvalid').style.display='none';
                    $('.md-trigger').trigger('click');
                }
                else  setTimeout( function() { location.replace("http://likebz.ru/pay/?step=two&pay="+data_type_pay);}, 100);
                /*
                 }
                 */
            }
        }
    });


    function emailvalid() { //валидация email при оставлении заявки через сайт
        document.getElementById('button_error').setAttribute('disabled', 'disabled');
        document.getElementById('error_text').innerHTML = '<b>Введите код подтверждения! <br/> Мы выслали код подтверждения<br/> на указанный вами email</b>';
        document.getElementById('button_error_text').innerHTML = 'Ввести';
        document.getElementById('inputemailvalid').style.display='block';
        setTimeout( function(){
            $(".md-trigger").trigger("click");
        },100);



        var input = document.getElementById("inputemailvalid");
        input.oninput = function() {

            var inputemailvalid = input.value;
            if(inputemailvalid.length == 4)  {
                var request_id = $_GET('request_id');
                var query ='request_id='+request_id+'&code='+$("#inputemailvalid").val();
                $.ajax({
                    type: 'POST',
                    url: 'http://likebz.ru/pay/emailvalid/emailvalid.php',
                    data: query,
                    success: function(data) {
                        if(data == "true")$('#button_error').removeAttr('disabled');
                        else alert("Код не верен!");
                    }
                });
            }
        };
    }


    function datavalid() {
        document.getElementById('button_error').setAttribute('disabled', 'disabled');
        document.getElementById('error_text').innerHTML = '<b>Введите код подтверждения! <br/> Мы выслали код подтверждения<br/> на указанный вами email</b>';
        document.getElementById('button_error_text').innerHTML = 'Ввести';
        document.getElementById('inputemailvalid').style.display='block';
        setTimeout( function(){
            $(".md-trigger").trigger("click");
        },100);



        var input = document.getElementById("inputemailvalid");
        input.oninput = function() {

            var inputemailvalid = input.value;
            if(inputemailvalid.length == 4)  {

            }
        };
    }




</script>



<script src="js/classie.js"></script>
<script src="js/modalEffects.js"></script>


<script>
    // this is important for IEs
    var polyfilter_scriptpath = '/js/';
</script>
<script src="js/cssParser.js"></script>
<script src="js/css-filters-polyfill.js"></script>




</body>
</html>
