<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HeadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'public/bootstrap/dist/css/bootstrap.min.css',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',

        'plugins/bower_components/timepicker/bootstrap-timepicker.min.css',
        'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css',

        'public/css/animate.css',
        'public/css/style.css',
        'public/css/main.css',
        'public/css/colors/default.css',
    ];
    public $js = [
        'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
        'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
    public $cssOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
