<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
            'identityCookie' => ['name' => '_identity-site', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/routing.php'),
        ],

    ],
    'modules' => [
        'users' => [
            'class' => 'backend\modules\users\Module',
        ],
        'partners' => [
            'class' => 'backend\modules\partners\Module',
        ],
        'products' => [
            'class' => 'backend\modules\products\Module',
        ],
        'leads' => [
            'class' => 'backend\modules\leads\Module',
        ],
        'bonus' => [
            'class' => 'backend\modules\bonus\Module',
        ],
        'outpay' => [
            'class' => 'backend\modules\outpay\Module',
        ],
        'config' => [
            'class' => 'backend\modules\config\Module',
        ],
    ],
    'params' => $params,
];
