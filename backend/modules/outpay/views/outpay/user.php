<?
$this->title = 'Выплаты';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@app/modules/partners/views/partners/view/head', array('model' => $user)) ?>

<div class="row">
    <?=$this->render('@app/modules/partners/views/partners/view/sidebar', array('model' => $user))?>

    <div class="col-md-10">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'ID',
                            'value' => function ($data) {
                                return $data->id;
                            },
                        ],
                        [
                            'label' => 'Партнер',
                            'format' => 'raw',
                            'content' => function ($data) {
                                return \yii\helpers\Html::a($data->users->name, \yii\helpers\Url::to(['/products/products/user', 'id' => $data->users_id]), []);
                            },
                        ],
                        [
                            'label' => 'Дата выплаты',
                            'value' => function ($data) {
                                return $data->created_at;
                            },
                        ],
                        [
                            'label' => 'Размер выплаты',
                            'value' => function ($data) {
                                return $data->value;
                            },
                        ],
                        [
                            'label' => 'Платежная система',
                            'value' => function ($data) {
                                return \common\models\PaymentsUser::$types[$data->payments_type];
                            },
                        ],
                        [
                            'label' => 'Номер для выплаты',
                            'value' => function ($data) {
                                return $data->purse_number;
                            },
                        ],
                        [
                            'label' => 'Статус ',
                            'value' => function ($data) {
                                if ($data->status == 1) {
                                    return 'в обработке';
                                }
                                if ($data->status == 2) {
                                    return 'выплачен';
                                }
                                if ($data->status == 3) {
                                    return 'не выплачен';
                                }
                            },
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
