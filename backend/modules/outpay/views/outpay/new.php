<?
$this->title = 'Выплаты';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<?=\yii\helpers\Html::submitInput('Выплатить отмеченным', ['class' => 'btn btn-success pull-right js_success'])?>
<?=\yii\helpers\Html::submitInput('Отменить выплату', ['class' => 'btn btn-danger pull-right js_error'])?>
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?= \yii\helpers\Html::beginForm([''], '', []) ?>
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'ID',
                            'value' => function ($data) {
                                return $data->id;
                            },
                        ],
                        [
                            'label' => 'Партнер',
                            'format' => 'raw',
                            'content' => function ($data) {
                                return \yii\helpers\Html::a($data->users->name, \yii\helpers\Url::to(['/products/products/user', 'id' => $data->users_id]), []);
                            },
                        ],
                        [
                            'label' => 'Дата выплаты',
                            'value' => function ($data) {
                                return $data->created_at;
                            },
                        ],
                        [
                            'label' => 'Размер выплаты',
                            'value' => function ($data) {
                                return $data->value;
                            },
                        ],
                        [
                            'label' => 'Платежная система',
                            'value' => function ($data) {
                                return \common\models\PaymentsUser::$types[$data->payments_type];
                            },
                        ],
                        [
                            'label' => 'Номер для выплаты',
                            'value' => function ($data) {
                                return $data->purse_number;
                            },
                        ],
                        [
                            'label' => 'Статус ',
                            'value' => function ($data) {
                                if ($data->status == 1) {
                                    return 'в обработке';
                                }
                                if ($data->status == 2) {
                                    return 'выплачен';
                                }
                                if ($data->status == 3) {
                                    return 'не выплачен';
                                }
                            },
                        ],
                        [
                            'label' => 'Выбрать',
                            'format' => 'raw',
                            'content' => function ($data) {
//                                return \yii\helpers\Html::checkbox('id[]',false, ['value' => $data->id]);
                                return '<div class="checkbox checkbox-success">
                                                <input id="checkbox'.$data->id.'" type="checkbox" name="id[]" value="'.$data->id.'">
                                                <label for="checkbox'.$data->id.'"> </label>
                                            </div>';
                            },
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
                <?= \yii\helpers\Html::endForm() ?>
            </div>
        </div>
    </div>
</div>

<?
$script = <<< JS
   $('.js_success').on('click', function () {
        $('form').attr('action', '/admin/outpay/outpay/success');
        $('form').submit();
   })
        
   $('.js_error').on('click', function () {
        $('form').attr('action', '/admin/outpay/outpay/error');
        $('form').submit();
   })
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>