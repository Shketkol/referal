<?php

namespace backend\modules\outpay\controllers;

use backend\components\controllers\AdminController;
use backend\models\InvoicesSearch;
use backend\models\PaymentsUsersSearch;
use common\models\Balance;
use common\models\Invoices;
use common\models\Leads;
use common\models\LeadsSearch;
use common\models\PaymentsUser;
use common\models\Users;
use yii\filters\AccessControl;
use Yii;
use yii\web\User;

/**
 * Finance controller for the `finance` module
 */
class OutpayController extends AdminController
{

    public function actionNew()
    {
        $searchModel = new PaymentsUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('new', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new PaymentsUsersSearch();
        $dataProvider = $searchModel->search_all(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUser($id)
    {
        $user = Users::findOne(['id' => $id]);
        $searchModel = new PaymentsUsersSearch();
        $dataProvider = $searchModel->search_user(Yii::$app->request->queryParams, $user->id);

        return $this->render('user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionSuccess(){
        if(Yii::$app->request->post()){
            $id_outpay = Yii::$app->request->post('id');
            foreach ($id_outpay as $value) {
                $balance = PaymentsUser::findOne(['id' => $value]);
                $balance->status = 2;
                $balance->save(false);
            }
            return $this->redirect('index');
        }
    }

    public function actionError(){
        if(Yii::$app->request->post()){
            $id_outpay = Yii::$app->request->post('id');
            foreach ($id_outpay as $value) {
                $balance = PaymentsUser::findOne(['id' => $value]);
                $balance->status = 3;
                $balance->save(false);

                $balance_user = Balance::findOne(['users_id' => $balance->users_id]);
                $balance_user->balance = (float)$balance_user->balance + (float)$balance->value;
                $balance_user->save(false);
            }
            return $this->redirect('index');
        }
    }
}
