<?php

namespace backend\modules\bonus\controllers;

use backend\components\controllers\AdminController;
use backend\models\InvoicesSearch;
use common\models\Invoices;
use common\models\Leads;
use common\models\LeadsSearch;
use common\models\Users;
use yii\filters\AccessControl;
use Yii;
use yii\web\User;

/**
 * Finance controller for the `finance` module
 */
class BonusController extends AdminController
{

    public function actionIndex(){
        $time = date('Y-m', time());
        $value = $time.'-1 - '.$time.'-'.date('t', time());
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post('Invoices');
            $value = $post['from_date'];
        }

        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search_bonus(Yii::$app->request->queryParams, $value);

        $model = new Invoices();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'value' => $value
        ]);
    }

    public function actionUser($id){
        $model = new Invoices();

        $time = date('Y-m', time());
        $value = $time.'-1 - '.$time.'-'.date('t', time());
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post('Invoices');
            $value = $post['from_date'];
        }

        $user = Users::findOne(['id' => $id]);

        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search_bonus_user(Yii::$app->request->queryParams, $id, $value);

        return $this->render('user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'value' => $value,
            'user' => $user
        ]);
    }

    public function actionFile(){
        $all = Invoices::find()
            ->where(['not', ['clients_id' => null]])
            ->all();
        $path = $_SERVER['DOCUMENT_ROOT'].'/export/sales.csv';
        $file1 = fopen($path, 'w');
        $str = "ID Партнера;Имя партнера;Продукт;Стоимость продукта;Размер вознаграждения;Клиент;Дата оплаты;Статус"."\n";
        fwrite($file1, $str);
        foreach ($all as $item){
            $str2 = $item->users_id.
                ';'.$item->users->name.
                ';'.$item->products->name.
                ';'.$item->price.
                ';'.$item->bonus.
                ';'.$item->clients->name.' '.$item->clients->email.' '.$item->clients->phone.
                ';'.$item->created_at;
            if($item->status == 1 || $item->status == 2){
                $str2 = $str2.';не оплачен'."\n";
            }elseif ($item->status == 3){
                $str2 = $str2.';оплачен'."\n";
            }
            fwrite($file1, $str2);
        }
        fclose($file1);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="sales.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        Yii::$app->end();
    }
}
