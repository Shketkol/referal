<?
$this->title = 'Продажи';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('navBlock'); ?>
<? echo \yii\helpers\Html::a('Скачать CSV ', \yii\helpers\Url::to(['/bonus/bonus/file']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-4">
        <div class="panel white-box">
            <? $form = \yii\bootstrap\ActiveForm::begin([
                'method' => 'post'
            ]); ?>
            <label class="control-label">Период</label>;
            <?= $form->field($model, 'from_date')->textInput(['class' => 'form-control input-daterange-datepicker', 'value' => !empty($value) ? $value : null])->label(false) ?>
            <button type="submit" class="btn btn-block btn-default">Применить</button>
            <? \yii\bootstrap\ActiveForm::end() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'label' => 'Партнер',
                            'format' => 'raw',
                            'content' => function ($data) {
                                if (is_null($data->users_id)) {
                                    return 'оплата без партнера';
                                } else {
                                    return \yii\helpers\Html::a($data->users->name, \yii\helpers\Url::to(['/products/products/user', 'id' => $data->users_id]), []);
                                }
                            },
                        ],
                        [
                            'label' => 'Продукт',
                            'value' => function ($data) {
                                return $data->products->name;
                            },
                        ],
                        [
                            'label' => 'Стоимость продукта',
                            'value' => function ($data) {
                                return $data->price;
                            },
                        ],
                        [
                            'label' => 'Размер вознаграждения',
                            'value' => function ($data) {
                                return $data->bonus;
                            },
                        ],
                        [
                            'label' => 'Клиент',
                            'content' => function ($data) {
                                return $data->clients->name . "<br>" . $data->clients->email . "<br>" . $data->clients->phone;
                            },
                        ],
                        [
                            'label' => 'Дата оплаты',
                            'value' => function ($data) {
                                return $data->created_at;
                            },
                        ],
                        [
                            'label' => 'Статус ',
                            'value' => function ($data) {
                                if ($data->status == 1 || $data->status == 2) {
                                    return 'не оплачен';
                                }
                                if ($data->status == 3) {
                                    return 'оплачен';
                                }
                            },
                        ]
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?
$script = <<< JS
   $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm']
        , applyClass: 'btn-danger'
        , cancelClass: 'btn-inverse',
        locale: {
            format: 'YYYY-MM-DD'
        },
        }); 
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
