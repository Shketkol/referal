<?php

namespace backend\modules\income\controllers;

use backend\components\controllers\AdminController;
use backend\models\IncomeFieldSearch;
use backend\models\OutGoSearch;
use backend\models\ProductSearch;
use backend\models\UsersSearch;
use common\models\Customers;
use common\models\IncomeField;
use common\models\IncomeUsers;
use common\models\Leads;
use common\models\Outgo;
use common\models\OutgoUsers;
use common\models\Products;
use common\models\Users;
use common\models\UsersProfile;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class IncomeController extends AdminController
{

    public function actionIndex($id)
    {
        $user = $this->loadModel($id);

        $active = true;
        if(!isset($_GET['search_income'])){
            $income = Leads::find()
                ->where(['users_id' => $user->id])
                ->orderBy('date DESC')
                ->one();
            if(!empty($income)){
                $month = date('m', strtotime($income->date));
                $year = date('Y', strtotime($income->date));
            }else {
                $month = date('m', time());
                $year = date('Y', time());
            }

        }else {
            $test = explode('-', $_GET['search_income']);
            $month = $test[0];
            $year = $test[1];
            $active = true;
        }
        $income_all = [];
        foreach (Products::find()->all() as $item) {
            $income = [
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between','date',$year.'-'.$month.'-1',$year.'-'.$month.'-5'])
                    ->andWhere(['products_id' => $item->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between','date',$year.'-'.$month.'-6',$year.'-'.$month.'-11'])
                    ->andWhere(['products_id' => $item->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between','date',$year.'-'.$month.'-12',$year.'-'.$month.'-18'])
                    ->andWhere(['products_id' => $item->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between','date',$year.'-'.$month.'-19',$year.'-'.$month.'-25'])
                    ->andWhere(['products_id' => $item->id])
                    ->asArray()
                    ->one(),
                Leads::find()
                    ->select('SUM(price) as price')
                    ->where(['between','date',$year.'-'.$month.'-26',$year.'-'.$month.'-31'])
                    ->andWhere(['products_id' => $item->id])
                    ->asArray()
                    ->one(),
            ];
            $income_all[$item->name] = $income;
        }
        $years_income = IncomeUsers::find()
            ->select('year')
            ->distinct()
            ->where(['users_id' => $user->id])
            ->orderBy('year DESC')
            ->all();
        $years_outgo = IncomeUsers::find()
            ->select('year')
            ->distinct()
            ->where(['users_id' => $user->id])
            ->orderBy('year DESC')
            ->all();


        $products = Outgo::find()
            ->where(['users_id' => $user->id])
            ->all();

        return $this->render('index', [
            'user' => $user,
            'income' => $income_all,
            'month' => $month,
            'year' => $year,
            'years_income' => $years_income,
            'years_outgo' => $years_outgo,
            'active' => $active,
            'products' => $products,
            'm' => $month,
            'y' => $year
        ]);
    }

    public function actionIncome($id)
    {
        $user = $this->loadModel($id);
        $searchModel = new IncomeFieldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('income_index', [
            'user' => $user,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIncomeCreate($id)
    {
        $model = $this->loadModel($id);
        $income = new IncomeField();
        if($income->load(Yii::$app->request->post()) && $income->save()){
            return $this->redirect(Url::to(['income', 'id' => $model->id]));
        }

        return $this->render('income_create', [
            'model' => $model,
            'income' => $income
        ]);
    }

    public function actionIncomeUpdate($id)
    {
        $income = IncomeField::findOne(['id' => $id]);
        $model = Users::findOne(['id' => $income->users_id]);
        if($income->load(Yii::$app->request->post()) && $income->save()){
            return $this->redirect(Url::to(['income', 'id' => $income->users_id]));
        }

        return $this->render('income_create', [
            'model' => $model,
            'income' => $income
        ]);
    }

    public function actionDelete($id)
    {
        $income = IncomeField::findOne(['id' => $id]);
        $income->delete();
        return $this->redirect(Url::to(['income', 'id' => $income->users_id]));
    }

    public function actionOut($id)
    {
        $user = $this->loadModel($id);
        $searchModel = new OutGoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('out_index', [
            'user' => $user,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOutCreate($id)
    {
        $model = $this->loadModel($id);
        $income = new Outgo();
        if($income->load(Yii::$app->request->post()) && $income->save()){
            return $this->redirect(Url::to(['out', 'id' => $model->id]));
        }

        return $this->render('out_create', [
            'model' => $model,
            'income' => $income
        ]);
    }

    public function actionOutUpdate($id)
    {
        $income = Outgo::findOne(['id' => $id]);
        $model = Users::findOne(['id' => $income->users_id]);
        if($income->load(Yii::$app->request->post()) && $income->save()){
            return $this->redirect(Url::to(['out', 'id' => $income->users_id]));
        }

        return $this->render('out_create', [
            'model' => $model,
            'income' => $income
        ]);
    }

    public function actionOutDelete($id)
    {
        $income = Outgo::findOne(['id' => $id]);
        $income->delete();
        return $this->redirect(Url::to(['out', 'id' => $income->users_id]));
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

}



