<?
use \yii\helpers\Html;
use \yii\helpers\Url;

$this->title = 'Доход/Расход';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@app/modules/partners/views/partners/view/head', array('model' => $user)) ?>
<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-plus m-r-5"></i> Редактировать расход ', Url::to(['/income/income/out', 'id' => $user->id]), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>
<div class="row">

    <?= $this->render('@app/modules/partners/views/partners/view/sidebar', array('model' => $user)) ?>

    <div class="col-md-10">
        <div class="panel">
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="<?=($active == true) ? 'active' : null?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Доход</span></a></li>
                            <li role="presentation" class="<?=($active == false) ? 'active' : null?>"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Расход</span></a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane <?=($active == true) ? 'fade active in' : null?>" id="home">
                                <div class="col-md-2 pull-right">
                                    <div class="form-group">
                                        <select class="form-control js_search_income">
                                            <option value="1" <?=($month == 1) ? 'selected' : null?>>январь</option>
                                            <option value="2" <?=($month == 2) ? 'selected' : null?>>февраль</option>
                                            <option value="3" <?=($month == 3) ? 'selected' : null?>>март</option>
                                            <option value="4" <?=($month == 4) ? 'selected' : null?>>апрель</option>
                                            <option value="5" <?=($month == 5) ? 'selected' : null?>>май</option>
                                            <option value="6" <?=($month == 6) ? 'selected' : null?>>июнь</option>
                                            <option value="7" <?=($month == 7) ? 'selected' : null?>>июль</option>
                                            <option value="8" <?=($month == 8) ? 'selected' : null?>>август</option>
                                            <option value="9" <?=($month == 9) ? 'selected' : null?>>сентябрь</option>
                                            <option value="10"<?=($month == 10) ? 'selected' : null?>>октябрь</option>
                                            <option value="11" <?=($month == 11) ? 'selected' : null?>>ноябрь</option>
                                            <option value="12" <?=($month == 12) ? 'selected' : null?>>декабрь</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pull-right">
                                    <div class="form-group">
                                        <select class="form-control js_search_income">
                                            <?if(!empty($years_income)){?>
                                            <?foreach ($years_income as $value){?>
                                                <option value="<?=$value->year?>" <?=($year == $value->year) ? 'selected' : null?>><?=$value->year?></option>
                                            <? }?>
                                            <? } else {?>
                                                <option value="<?=date('Y', time())?>" <?=($year == date('Y', time())) ? 'selected' : null?>><?=date('Y', time())?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                    <tr>
                                        <th>Поля</th>
                                        <th>0-5</th>
                                        <th>6-11</th>
                                        <th>12-18</th>
                                        <th>19-25</th>
                                        <th>26-31</th>
                                        <th width="300">ИТОГО ЗА МЕСЯЦ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?$all=0;?>
                                    <?foreach ($income as $key=>$value){?>
                                        <?$temp = 0;?>
                                        <tr>
                                            <td><?=$key?></td>
                                            <?foreach ($value as $val){?>
                                                <? $t = (empty($val['price'])) ? '0.00' : $val['price']; ?>
                                                <td><?=$t?></td>
                                                <? $temp = $temp + $t; ?>
                                            <? } ?>
                                            <td><?=number_format($temp, 2, '.', '')?></td>
                                        </tr>
                                        <? $all = $all + $temp;?>
                                    <? } ?>
                                    </tbody>
                                </table>
                                <h4 class="page-title">Всего: <?=number_format($all, 2, '.', '')?></h4>
                            </div>
                            <div role="tabpanel" class="tab-pane <?=($active == false) ? 'fade active in' : null?>" id="profile">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                    <tr>
                                        <th>Поля</th>
                                        <th>0-5</th>
                                        <th>6-11</th>
                                        <th>12-18</th>
                                        <th>19-25</th>
                                        <th>26-31</th>
                                        <th width="300">ИТОГО ЗА МЕСЯЦ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?$all1=0;?>
                                    <?foreach ($products as $value){?>
                                        <tr>
                                            <th><?=$value->products->name?></th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                        <?$fields = \common\models\Outgo::find()->where(['products_id' => $value->products_id])->all()?>
                                        <?$temp = 0;?>
                                        <?foreach ($fields as $v){?>
                                            <tr>
                                            <th><?=$v->name?></th>
                                            <?for($i=1; $i<=5; $i++){?>
                                                <? $t = \common\models\OutgoUsers::getIncome($user->id,
                                                    $m, $y, $v->id, $i); ?>
                                                <? $d = (empty($t)) ? '0.00' : $t->value;?>
                                                <td><a href="<?=(!empty($t)) ? Url::to(['outgo-update', 'id' => $t->id]) : '#'?>"><?=$d;?></a></td>
                                                <? $temp = $temp + $d; ?>
                                            <? }?>
                                        <? } ?>
                                        <td><?=number_format($temp, 2, '.', '')?></td>
                                        <? $all1 = $all1 + $temp;?>
                                        </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                                <h4 class="page-title">Всего: <?=number_format($all1, 2, '.', '')?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="white-box">
                            <div class="text-center">
                                <h1>Прибыль: <?=$all-$all1?></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>