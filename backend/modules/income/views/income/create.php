<?
$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@app/modules/partners/views/partners/view/head', array('model' => $model)) ?>

<div class="row">

    <?= $this->render('@app/modules/partners/views/partners/view/sidebar', array('model' => $model)) ?>

    <div class="col-md-10">
        <div class="row">
        <div class="panel">

            <div class="table-responsive">
                <? $form = \yii\widgets\ActiveForm::begin([
                    'enableClientValidation' => false,
                    'validateOnSubmit' => true,
                    'options' => [
                        'class' => 'form-horizontal',
                        'autocomplete' => 'off',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                        'labelOptions' => ['class' => 'col-md-12'],
                    ],
                ]); ?>
                <?= $form->errorSummary($product); ?>
                <?= $form->field($product, 'users_id')->hiddenInput(['value' => $model->id, 'required' => true])->label(false) ?>
                <?= $form->field($product, 'name')->textInput(['required' => true]) ?>
                <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp Сохранить &nbsp;</button>
                <? \yii\widgets\ActiveForm::end(); ?>
            </div>
            </div>
        </div>
    </div>
</div>
