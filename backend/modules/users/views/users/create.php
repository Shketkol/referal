<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'action' => Url::to(['create']),
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                'labelOptions' => ['class' => 'col-md-12'],
            ],
        ]); ?>
        <div class="white-box">
            <div class="row">
                <div class="col-xs-9">
                    <h3 class="box-title m-b-0">Основная информация</h3>
                </div>
            </div>
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true]) ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true]) ?>
                    <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true]) ?>
                    <?= $form->field($model, 'patronymic')->textInput(['placeholder' => 'Отчество', 'required' => true]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true]) ?>
                    <?= $form->field($model, 'city')->textInput(['placeholder' => 'Город', 'required' => true]) ?>
                    <?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true]) ?>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>