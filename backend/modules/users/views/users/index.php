<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-plus m-r-5"></i> Пригласить ', Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'label' => 'Партнер',
                            'content' => function($model) {
                                return $model->name."<br>".$model->usersProfile->surname."<br>".$model->usersProfile->patronymic;
                            },

                        ],
                        'email',
                        [
                            'attribute' => 'phone',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'label' => 'Выручка',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'label' => 'Количество продаж',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'label' => 'Сумма комиссий',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'label' => 'Комиссия за период',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'filter' => Url::to(['index']),
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<i class="ti-pencil" aria-hidden="true"></i>', Url::to(['update', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
