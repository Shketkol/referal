<?php

namespace backend\modules\products\controllers;

use backend\components\controllers\AdminController;
use backend\models\ProductSearch;
use backend\models\UsersSearch;
use common\models\Customers;
use common\models\Products;
use common\models\Users;
use common\models\UsersProfile;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class ProductsController extends AdminController
{

    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUser($id){
        $user = Users::findOne(['id' => $id]);

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionCreate()
    {
        $product = new Products();
        if($product->load(Yii::$app->request->post())){
            $product->token = Yii::$app->security->generateRandomString() . '_' . time();
            if($product->save()){
                return $this->redirect(Url::to(['index']));
            }
        }

        return $this->render('create', [
            'product' => $product
        ]);
    }

    public function actionUpdate($id)
    {
        $product = Products::findOne(['id' => $id]);
        if($product->load(Yii::$app->request->post()) && $product->save()){
            return $this->redirect(Url::to(['index']));
        }

        return $this->render('create', [
            'product' => $product
        ]);
    }

}



