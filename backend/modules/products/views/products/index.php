<?
$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo \yii\helpers\Html::a('<i class="fa fa-edit m-r-5"></i> Добавить', \yii\helpers\Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="col-md-2 pull-right">
<!--                --><?// echo \yii\helpers\Html::a('<i class="fa fa-plus m-r-5"></i> Добавить ', \yii\helpers\Url::to(['/products/products/create']), [
//                    'class' => 'btn btn-success m-l-10 waves-effect waves-light'
//                ]) ?>
            </div>
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        'name',
                        'bonus',
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return \yii\helpers\Html::a('<i class="ti-pencil" aria-hidden="true"></i>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>