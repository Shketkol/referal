<?php

namespace backend\modules\leads\controllers;

use backend\components\controllers\AdminController;
use backend\models\InvoicesSearch;
use common\models\Invoices;
use common\models\Leads;
use common\models\LeadsSearch;
use common\models\Users;
use yii\filters\AccessControl;
use Yii;
use yii\web\User;

/**
 * Finance controller for the `finance` module
 */
class LeadsController extends AdminController
{

    public function actionIndex(){
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUser($id){
        $user = Users::findOne(['id' => $id]);
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search_user(Yii::$app->request->queryParams, $user->id);

        return $this->render('user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user
        ]);
    }

    public function actionFile(){
        $all = Invoices::find()
            ->where(['not', ['clients_id' => null]])
            ->all();
        $path = $_SERVER['DOCUMENT_ROOT'].'/export/leads.csv';
        $file1 = fopen($path, 'w');
        $str = "Имя;Email;Телефон;Продукт;Дата регистрации;Статус"."\n";
        fwrite($file1, $str);
        foreach ($all as $item){
            $str2 = $item->clients->name.
                ';'.$item->clients->email.
                ';'.$item->clients->phone.
                ';'.$item->products->name.
                ';'.$item->created_at;

            if($item->status == 1 || $item->status == 2){
                $str2 = $str2.';не оплачен'."\n";
            }elseif ($item->status == 3){
                $str2 = $str2.';оплачен'."\n";
            }
            fwrite($file1, $str2);
        }
        fclose($file1);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="leads.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        Yii::$app->end();
    }
}
