<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Фин. планирование';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                'labelOptions' => ['class' => 'col-md-12'],
            ],
        ]); ?>
        <div class="white-box">
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'users_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                    <?= $form->field($model, 'year')->textInput(['required' => true]) ?>
                    <?= $form->field($model, 'name')->textInput(['required' => true]) ?>
                    <?=\frontend\widgets\mylti\Myltiwidgets::widget([
                        'form' => $form,
                        'name' => 'Поля раздела',
                        'model' => $model,
                        'attribute' => 'content',
                        'fields' => [
                            'name_roz' => [
                                'label' => 'Названия раздела',
                            ],
                            'jan' => [
                                'label' => 'январь',
                            ],
                            'jan_proz' => [
                                'label' => 'январь,%',
                            ],
                            'feb' => [
                                'label' => 'февраль',
                            ],
                            'feb_proz' => [
                                'label' => 'февраль,%',
                            ],
                            'march' => [
                                'label' => 'март',
                            ],
                            'march_proz' => [
                                'label' => 'март,%',
                            ],
                            'april' => [
                                'label' => 'апрель',
                            ],
                            'april_proz' => [
                                'label' => 'апрель,%',
                            ],
                            'may' => [
                                'label' => 'май',
                            ],
                            'may_proz' => [
                                'label' => 'май,%',
                            ],
                            'june' => [
                                'label' => 'июнь',
                            ],
                            'june_proz' => [
                                'label' => 'июнь,%',
                            ],
                            'jule' => [
                                'label' => 'июль',
                            ],
                            'jule_proz' => [
                                'label' => 'июль,%',
                            ],
                            'augest' => [
                                'label' => 'август',
                            ],
                            'augest_proz' => [
                                'label' => 'август,%',
                            ],
                            'sept' => [
                                'label' => 'сентябрь',
                            ],
                            'sept_proz' => [
                                'label' => 'сентябрь,%',
                            ],
                            'okt' => [
                                'label' => 'октябрь',
                            ],
                            'okt_proz' => [
                                'label' => 'октябрь,%',
                            ],
                            'nov' => [
                                'label' => 'ноябрь',
                            ],
                            'nov_proz' => [
                                'label' => 'ноябрь,%',
                            ],
                            'dec' => [
                                'label' => 'декабрь',
                            ],
                            'dec_proz' => [
                                'label' => 'декабрь,%',
                            ],
                        ]

                    ])?>

                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>