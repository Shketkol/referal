<?php

namespace frontend\modules\planning\controllers;

use common\models\Planing;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use Yii;

/**
 * Default controller for the `planning` module
 */
class PlanningController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $year = Planing::find()
            ->orderBy('year DESC')
            ->one();
        $models = Planing::find()
            ->where(['users_id' => Yii::$app->user->id, 'year' =>$year->year])
            ->all();
        $years = Planing::find()
            ->distinct()
            ->orderBy('year DESC')
            ->all();

        return $this->render('index', [
            'models' => $models,
            'years' => $years
        ]);
    }

    public function actionCreate(){
        $model = new Planing();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = Planing::findOne(['id' => $id]);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
}
