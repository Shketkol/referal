<?
$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <? $form = \yii\widgets\ActiveForm::begin([
                'enableClientValidation' => false,
                'validateOnSubmit' => true,
                'options' => [
                    'class' => 'form-horizontal',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <?= $form->errorSummary($model); ?>
            <?= $form->field($model, 'value')->textInput(['required' => true]) ?>
            <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i
                    class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp Сохранить &nbsp;
            </button>
            <? \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>
</div>
