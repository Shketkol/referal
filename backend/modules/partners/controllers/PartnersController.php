<?php

namespace backend\modules\partners\controllers;

use backend\components\controllers\AdminController;
use backend\models\ProductSearch;
use backend\models\UsersSearch;
use common\models\Customers;
use common\models\Invoices;
use common\models\Users;
use common\models\UsersProfile;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class PartnersController extends AdminController
{

    public function actionIndex()
    {
        $time = date('Y-m', time());
        $period = $time.'-1 - '.$time.'-'.date('t', time());
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post('Invoices');
            $period = $post['from_date'];
        }

        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search_user(Yii::$app->request->queryParams, $period);

        $model = new Invoices();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'period' => $period
        ]);
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionProduct($id){
        $model = $this->loadModel($id);
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('products', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

    public function actionFile(){
        $all = Invoices::find()
            ->where(['status' => 3])
            ->all();
        $path = $_SERVER['DOCUMENT_ROOT'].'/export/partners.csv';
        $file1 = fopen($path, 'w');
        $str = "ID Пользователя;Имя;Фамилия;Отчество;Email;Телефон;Выручка;Количество продаж;Сумма комиссий;Комиссия;Дата"."\n";
        fwrite($file1, $str);
        foreach ($all as $item){
            $str2 = $item->users_id.
                ';'.$item->users->name.
                ';'.$item->users->usersProfile->surname.
                ';'.$item->users->usersProfile->patronymic.
                ';'.$item->users->email.
                ';'.$item->users->usersProfile->phone.
                ';'.Invoices::getSumPrice($item->users_id).
                ';'.Invoices::getCountPay($item->users_id).
                ';'.Invoices::getSumBonus($item->users_id).
                ';'.$item->bonus.
                ';'.$item->created_at."\n";
            fwrite($file1, $str2);
        }
        fclose($file1);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="partners.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        Yii::$app->end();
    }

}



