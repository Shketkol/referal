<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Партнеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('Скачать CSV ', Url::to(['/partners/partners/file']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-4">
        <div class="panel white-box">
            <?$form = \yii\bootstrap\ActiveForm::begin([
                'method' => 'post'
            ]);?>
            <label class="control-label">Период</label>;
            <?=$form->field($model, 'from_date')->textInput(['class' => 'form-control input-daterange-datepicker', 'value' => !empty($period) ? $period : null])->label(false)?>
            <button type="submit" class="btn btn-block btn-default">Применить</button>
            <?\yii\bootstrap\ActiveForm::end()?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'label' => 'Партнер',
                            'content' => function($model) {
                                return $model->name."<br>".$model->usersProfile->surname."<br>".$model->usersProfile->patronymic;
                            },

                        ],
                        'email',
                        [
                            'attribute' => 'phone',
                            'value' => function($model) {
                                return $model->usersProfile->phone;
                            },

                        ],
                        [
                            'label' => 'Выручка',
                            'value' => function($model) {
                                return \common\models\Invoices::getSumPrice($model->id);
                            },

                        ],
                        [
                            'label' => 'Количество продаж',
                            'value' => function($model) {
                                return \common\models\Invoices::getCountPay($model->id);
                            },

                        ],
                        [
                            'label' => 'Сумма комиссий',
                            'value' => function($model) {
                                return \common\models\Invoices::getSumBonus($model->id);
                            },

                        ],
                        [
                            'label' => 'Комиссия за период',
                            'value' => function ($model)use ($period) {
                                $period_new = (!empty($period)) ? $period : null;
                                return \common\models\Invoices::getSumBonusPeriod($model->id, $period_new);
                            },

                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'filter' => Url::to(['index']),
                            'template' => '{view}{update}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="ti-eye" aria-hidden="true"></i>', \yii\helpers\Url::to(['/products/products/user', 'id' => $model->id]), [
                                        'data-original-title' => 'Просмотр',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a('<i class="ti-pencil" aria-hidden="true"></i>', Url::to(['/users/users/update', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
<?
$script = <<< JS
   $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm']
        , applyClass: 'btn-danger'
        , cancelClass: 'btn-inverse',
        locale: {
            format: 'YYYY-MM-DD'
        },
        }); 
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>