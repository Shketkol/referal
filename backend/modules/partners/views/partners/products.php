<?
$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('view/head', array('model' => $model)) ?>

<div class="row">

    <?= $this->render('view/sidebar', array('model' => $model)) ?>

    <div class="col-md-9">
        <div class="panel">
            <? echo \yii\helpers\Html::a('<i class="fa fa-plus m-r-5"></i> Добавить ', \yii\helpers\Url::to(['/partners/partners/product']), [
                'class' => 'btn btn-success m-l-10 waves-effect waves-light'
            ]) ?>
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        'name',
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'template' => '{edit}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="ti-pensil" aria-hidden="true"></i>', Url::to(['view', 'id' => $model->id]), [
                                        'data-original-title' => 'Редактировать',
                                        'data-toggle' => 'tooltip',
                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>