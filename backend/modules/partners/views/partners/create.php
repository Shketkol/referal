<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;
use \common\models\UsersRoles;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Пригласить в систему';
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-bars m-r-5"></i> Список ', Url::to(['index']), [
    'class' => 'btn btn-default btn-outline pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="box-title m-b-0">Пригласить в систему</h3>
                    <p class="text-muted m-b-30 font-13">На указанные e-mail адреса будут отправлены ссылки для завершения регистрации в системе.</p>
                </div>
            </div>

            <? $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'validateOnSubmit' => true,
                'action' => Url::to(['create']),
                'options' => [
                    'class' => 'form-horizontal',
                    'ng-controller' => 'usersController',
                    'name' => 'form',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <?= $form->errorSummary($model); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group required">
                        <label class="col-md-12" ng-model="customers_id_init">Клиент</label>
                        <div class="col-md-12">
                            <div class="typeahead__container">
                                <div class="typeahead__field input-group">
                                    <span class="typeahead__query">
                                        <input class="typeahead form-control" type="text"
                                           placeholder="Введите название или ID клиента" name="Users[customers_id]"
                                           ng-model="customers_id"
                                           required autocomplete="off" ng-init="customers_id_init=false">
                                    </span>
                                    <span class="input-group-btn">
                                        <a href="<?= Url::to(['/customers/customers/index']) ?>"
                                           target="_blank"
                                           class="btn btn-default btn-sml waves-effect waves-light"
                                           data-toggle="tooltip" title="Все клиенты"><i
                                                    class="fa fa-bars"></i></a>
                                    </span>
                                </div>
                            </div>
                            <span class="help-block hidden"></span>
                            <span class="help-block"><small>Пользователи будут представлять выбранного клиента.</small></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <? $role = UsersRoles::findOne(['type' => Users::TYPE_USER])->id ?>
                    <?= $form->field($model, 'users_roles_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(UsersRoles::find()->where(['type' => Users::TYPE_USER])->all(), 'id', 'name'),
                        array('ng-model' => 'role', 'ng-init' => "role='$role'"))->hint('Пользователи будут приглашеный в выбранной роли.'); ?>
                </div>
            </div>

            <div ng-repeat="user in users" ng-class="{'row': $first, 'row hidden-xs': $index}">
                <div class="col-sm-3">
                    <div ng-class="{'form-group': user.error!=true, 'form-group has-error': user.error==true}">
                        <label class="col-md-12" for="example-email" ng-if="$first">E-mail</label>
                        <div class="col-md-12">
                            <input type="email" class="form-control" placeholder="E-mail" ng-change="change()"
                                   ng-model="user.email">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="col-md-12" ng-if="$first">Имя</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="не обязательно" ng-model="user.name"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="col-md-12" ng-if="$first">Фамилия</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="не обязательно" ng-model="user.surname"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="col-md-12" ng-if="$first">Отчество</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="не обязательно" ng-model="user.patronymic"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row m-t-10">
                <div class="col-sm-12">
                    <p class="">Персонифицировать приглашение добавив к нему <a href="javascript:void(0)"
                                                                                class="text-primary"
                                                                                ng-click="show=true">сообщение от себя</a></p>
                </div>
                <div class="col-sm-12" ng-show="show">
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea class="form-control" rows="5" placeholder="Сообщение от себя..." ng-model="comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <button ng-click="new_user($event, form, users, comment, customers_id, role)" type="submit"
                    class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"
                    ng-if="users[0].email != '' && customers_id_init == true"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Пригласить {{counter-1}} пользователей &nbsp;</button>
            <button type="submit"
                    class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10 disabled"
                    ng-if="users[0].email == '' || customers_id_init == false"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Пригласить пользователей &nbsp;</button>

            <? ActiveForm::end(); ?>
        </div>

    </div>
</div>