<div class="col-md-2">
    <div class="panel wallet-widgets">
        <ul class="wallet-list">
            <li><a href="<?=\yii\helpers\Url::to(['/products/products/user', 'id' => $model->id])?>">
                    <div class="text-center">
                        Продукты
                    </div>
                </a>
            </li>
            <li><a href="<?=\yii\helpers\Url::to(['/leads/leads/user', 'id' => $model->id])?>">
                    <div class="text-center">
                        Клиенты
                    </div>
                </a>
            </li>
            <li><a href="<?=\yii\helpers\Url::to(['/bonus/bonus/user', 'id' => $model->id])?>">
                    <div class="text-center">
                        Начисления
                    </div>
                </a>
            </li>
            <li><a href="<?=\yii\helpers\Url::to(['/outpay/outpay/user', 'id' => $model->id])?>">
                    <div class="text-center">
                        Выплаты
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>