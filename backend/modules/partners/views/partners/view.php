<?
$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<?=$this->render('view/head', array('model' => $model))?>

<div class="row">

    <?=$this->render('view/sidebar', array('model' => $model))?>

    <div class="col-md-10">
        <div class="panel wallet-widgets">
            <div class="panel-body">
                <div class="text-left pull-left">
                    <h1>Наличная выручка:</h1>
                </div>
                <div class="text-right">
                    <h1> $5,000</h1>
                </div>
            </div>
            <ul class="wallet-list">
                <li><a href="javascript:void(0)">
                        <div class="text-left pull-left">
                            Концентрат:
                        </div>
                        <div class="text-right">
                            $5,000
                        </div>
                    </a>
                </li>
                <li><a href="javascript:void(0)">
                        <div class="text-left pull-left">
                            Долина:
                        </div>
                        <div class="text-right">
                            $5,000
                        </div>
                    </a>
                </li>
                <li><a href="javascript:void(0)">
                        <div class="text-left pull-left">
                            Маштабиравание:
                        </div>
                        <div class="text-right">
                            $5,000
                        </div>
                    </a>
                </li>
                <li><a href="javascript:void(0)">
                        <div class="text-left pull-left">
                            Акселерация:
                        </div>
                        <div class="text-right">
                            $5,000
                        </div>
                    </a>
                </li>
                <li><a href="javascript:void(0)">
                        <div class="text-left pull-left">
                            Армия:
                        </div>
                        <div class="text-right">
                            $5,000
                        </div>
                    </a>
                </li>
            </ul>
            <button class="btn btn-block btn-success">ДОБАВИТЬ</button>
        </div>
    </div>
</div>