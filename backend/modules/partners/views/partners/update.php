<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = (!empty($model->name))?$model->surname.' '.$model->name.' '.$model->patronymic:'Пользователь';
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-bars m-r-5"></i> Список ', Url::to(['index']), [
    'class' => 'btn btn-default btn-outline pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12" ng-controller="usersController">

        <div class="white-box">
            <div class="row">
                <div class="col-xs-9">
                    <h3 class="box-title m-b-0">Карточка пользователя</h3>
                    <p class="text-muted m-b-30 font-13"> Поля отмеченные звездочкой * обязательны для заполнения. </p>
                </div>
                <div class="col-xs-3">
                    <div class="btn-group pull-right">
                        <button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline waves-effect waves-light" type="button">Действия <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu">
                            <?if($model->status == Users::STATUS_INVITED){?>
                                <li><a href="<?=Url::to(['invite', 'id' => $model->id])?>" onclick="return false;" ng-click="invite($event)" class="js_invite">Отправить приглашение повторно</a></li>
                                <li class="divider"></li>
                            <?}?>
                            <li><a href="<?=Url::to(['delete', 'id' => $model->id])?>" onclick="return false" ng-click="delete($event)" class="js_delete" data-title="Вы уверенны?" data-text="Вы собираетесь удалить пользователя" >Удалить пользователя</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?if(empty($model->customers_id) && $model->status == Users::STATUS_REQUESTED && !empty($model->company)):?>
                <blockquote>
                    <small>При регистрации пользователь указал, что представляет компанию: <strong><?=$model->company?></strong></small>
                </blockquote>
            <?endif;?>

            <? $form = ActiveForm::begin([
                'id' => 'users-update',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
                'action' => Url::to(['update', 'id' => $model->id]),
                'options' => [
                    'class' => 'form-horizontal',
                    'name' => 'form',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <?=$form->errorSummary($model);?>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12">Статус пользователя</label>

                        <div class="col-md-12 radio-list">
                            <?if($model->status == Users::STATUS_INVITED):?>
                                <label class="radio-inline p-l-0">
                                    <div class="radio radio-warning">
                                        <input type="radio" name="Users[status]" <?=($model->status == Users::STATUS_INVITED) ? 'ng-init="user.status='.Users::STATUS_INVITED.'"': null?> id="radio4" value="<?=Users::STATUS_INVITED?>" ng-model="user.status">
                                        <label for="radio4"><?=Users::$Statuses[Users::STATUS_INVITED]?></label>
                                    </div>
                                </label>
                            <? endif; ?>

                            <?if($model->status != Users::STATUS_INVITED):?>
                            <label class="radio-inline p-l-0">
                                <div class="radio radio-success">
                                    <input type="radio" name="Users[status]" <?=($model->status == Users::STATUS_ACTIVE) ? 'ng-init="user.status='.Users::STATUS_ACTIVE.'"': null?> id="radio1" value="<?=Users::STATUS_ACTIVE?>" ng-model="user.status">
                                    <label for="radio1"><?=Users::$Statuses[Users::STATUS_ACTIVE]?></label>
                                </div>
                            </label>
                            <? endif; ?>

                            <?if($model->status == Users::STATUS_ACTIVE || $model->status == Users::STATUS_DEACTIVATED):?>
                            <label class="radio-inline">
                                <div class="radio radio-default">
                                    <input type="radio" name="Users[status]" <?=($model->status == Users::STATUS_DEACTIVATED) ? 'ng-init="user.status='.Users::STATUS_DEACTIVATED.'"': null?> id="radio2" value="<?=Users::STATUS_DEACTIVATED?>" ng-model="user.status">
                                    <label for="radio2"><?=Users::$Statuses[Users::STATUS_DEACTIVATED]?></label>
                                </div>
                            </label>
                            <? endif; ?>

                            <?if($model->status == Users::STATUS_REQUESTED):?>
                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="Users[status]" <?=($model->status == Users::STATUS_REQUESTED) ? 'ng-init="user.status='.Users::STATUS_REQUESTED.'"': null?> id="radio3" value="<?=Users::STATUS_REQUESTED?>" ng-model="user.status">
                                    <label for="radio3"><?=Users::$Statuses[Users::STATUS_REQUESTED]?></label>
                                </div>
                            </label>
                            <? endif; ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'ng-model' => 'user.email', 'use-form-data' => true, 'readonly' => ($model->status == Users::STATUS_INVITED) ? false : true, 'required' => true]) ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'ng-model' => 'user.name', 'use-form-data' => true,]) ?>
                    <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'ng-model' => 'user.surname', 'use-form-data' => true]) ?>
                    <?= $form->field($model, 'patronymic')->textInput(['placeholder' => 'Отчество', 'ng-model' => 'user.patronymic', 'use-form-data' => true]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'ng-model' => 'user.phone', 'use-form-data' => true]) ?>

                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'id')->textInput(['ng-model' => 'user.id', 'use-form-data' => true, 'readonly' => true]) ?>
                    <?= $form->field($model, 'created_at')->textInput(['ng-model' => 'user.created_at', 'value' => date('d.m.Y H:i', strtotime($model->created_at)), 'use-form-data' => true, 'readonly' => true]) ?>

                    <? $role = (empty($model->users_roles_id)) ? \common\models\UsersRoles::findOne(['type' => Users::TYPE_USER])->id : $model->users_roles_id; ?>
                    <?= $form->field($model, 'users_roles_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\UsersRoles::find()->where(['type' => Users::TYPE_USER])->all(), 'id', 'name'),
                        array('ng-model' => 'user.role',  'ng-init' => "user.role='$role'")
                    ) ?>

                    <div class="form-group">
                        <label class="col-md-12" ng-model="customers_id_init">Клиент</label>
                        <div class="col-md-12">
                            <?if(empty($model->customers->name)): ?>
                                <div class="typeahead__container">
                                    <div class="typeahead__field input-group">
                                        <span class="typeahead__query">
                                        <input class="typeahead-update form-control" type="text"
                                               placeholder="Введите название или ID клиента" name="Users[customers_id]"
                                               id="users-customers_id"
                                               ng-model="user.customers_id"
                                               autocomplete="off">
                                        </span> {{user.customers_id}}
                                        <span class="input-group-btn">
                                            <a href="<?= Url::to(['/customers/customers/index']) ?>"
                                               target="_blank"
                                               class="btn btn-default btn-sml waves-effect waves-light"
                                               data-toggle="tooltip" title="Все клиенты"><i
                                                        class="fa fa-bars"></i></a>
                                        </span>
                                    </div>
                                </div>
                            <?else:?>
                                <div class="input-group">
                                    <input class="form-control" type="text" value="<?=$model->customers->name?>" readonly>
                                    <span class="input-group-btn">
                                        <a href="<?= Url::to(['/customers/customers/update','id'=>$model->customers_id]) ?>" target="_blank" class="btn btn-default btn-sml waves-effect waves-light" data-toggle="tooltip" title="Карточка клиента"><i class="fa fa-info"></i></a>
                                    </span>
                                </div>
                            <?endif;?>
                            <span class="help-block hidden"></span>
                        </div>
                    </div>

                    <?= $form->field($model, 'post')->textInput(['placeholder' => 'Должность', 'ng-model' => 'user.post', 'use-form-data' => true]) ?>

                </div>
            </div>

            <button type="submit" ng-click="update_user($event, form, user)"
                    class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
