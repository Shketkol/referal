<?php
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;
/* @var $this yii\web\View */

$this->title = 'Франшиза';
?>
<!--<div class="row">-->
<!--    <div class="col-sm-12">-->
<!--        <div class="white-box">-->
<!--            <div class="row row-in">-->
<!--                <div class="col-lg-6 col-sm-6 row-in-br">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-success"><i class=" ti-shopping-cart"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=$users?><!--</h3></li>-->
<!--                        <li class="col-middle">-->
<!--                            <h4>Количество франшази</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-6 col-sm-6  b-0">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=(empty($sum['price'])) ? 0.00 : $sum['price']?><!--</h3></li>-->
<!--                        <li class="col-middle">-->
<!--                            <h4>Сумма выручки</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="col-md-12">-->
<!--        <div class="white-box">-->
<!--            <h1 class="box-title">Рейтинг франшази</h1>-->
<!--            <div class="table-responsive">-->
<!--                --><?php //\yii\widgets\Pjax::begin(); ?>
<!--                --><?//= GridView::widget([
//                    'dataProvider' => $dataProvider,
//                    'summary' => false,
//                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
//                    'tableOptions' => [
//                        'class' => 'table table-hover manage-u-table'
//                    ],
//                    'columns' => [
//                        [
//                            'attribute'=>'id',
//                            'contentOptions' =>['class' => 'text-center'],
//                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
//                        ],
//
//                        'name',
//                        [
//                            'attribute' => 'surname',
//                            'content' => function($model) {
//                                return '<a href="'.Url::to(['/partners/partners/view', 'id' => $model->id]).'">'.$model->surname.'</a>';
//                            },
//
//                        ],
//                        [
//                            'attribute' => 'patronymic',
//                            'value' => function($model) {
//                                return $model->patronymic;
//                            },
//
//                        ],
//                        'email',
//                        'price',
//                    ],
//                ]); ?>
<!--                --><?php //\yii\widgets\Pjax::end(); ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
