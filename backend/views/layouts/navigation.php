<?
use \yii\helpers\Url;
?>
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Навигация</span></h3> </div>
        <ul class="nav" id="side-menu">
<!--            <li> <a href="--><?//=\yii\helpers\Url::to(['/site/index'])?><!--" class="waves-effect"><i class="mdi mdi-gauge fa-fw"></i><span class="hide-menu">Главная панель</span></a></li>-->
            <li> <a href="<?=\yii\helpers\Url::to(['/products/products/index'])?>" class="waves-effect"><i class="mdi mdi-book-multiple fa-fw"></i><span class="hide-menu">Продукты</span></a></li>
            <li> <a href="<?=\yii\helpers\Url::to(['/partners/partners/index'])?>"><i class="mdi mdi-account-star fa-fw"></i><span class="hide-menu">Партнеры</span></a> </li>
            <li> <a href="<?=\yii\helpers\Url::to(['/leads/leads/index'])?>"><i class="mdi mdi-account-multiple fa-fw"></i><span class="hide-menu">Лиды</span></a> </li>
            <li> <a href="<?=\yii\helpers\Url::to(['/bonus/bonus/index'])?>"><i class="mdi mdi-sale fa-fw"></i><span class="hide-menu">Продажи</span></a> </li>


            <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-cash-100 fa-fw"></i> <span class="hide-menu">Выплаты<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?=\yii\helpers\Url::to(['/outpay/outpay/new'])?>"><i class="mdi mdi-new-box fa-fw"></i><span class="hide-menu">Новые</span></a> </li>
                    <li> <a href="<?=\yii\helpers\Url::to(['/outpay/outpay/index'])?>"><i class="mdi mdi-cash-multiple fa-fw"></i><span class="hide-menu">Обработаные</span></a> </li>
                </ul>
            </li>

            <li><a href="<?=Url::to(['/logout'])?>" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Выйти</span></a></li>
        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
<!-- ============================================================== -->