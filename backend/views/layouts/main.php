<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\MainAsset;
use backend\assets\HeadAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

HeadAsset::register($this);
MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="myApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" sizes="16x16" href="/plugins/images/favicon.png">
</head>
<body>
<?php $this->beginBody() ?>

<?=\frontend\widgets\alert\MessageWidget::widget()?>

<?=$this->render('header')?>

<?=$this->render('navigation')?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <?=(isset($this->blocks['navBlock']))?$this->blocks['navBlock']:'' ?>
                <?
                echo Breadcrumbs::widget([
                    'options' => ['class' => 'breadcrumb m-r-10'],
                    'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
                ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <?=$content?>

    </div>

    <?=$this->render('footer')?>
</div>



<?
$script = <<< JS
(function () {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
            new CBPFWTabs(el);
        });
    })();
// // jQuery(document).ready(function () {
//         $("input[name='tch3']").TouchSpin({
//             min: 1,
//             max: 9999,
//             buttondown_class: 'btn btn-info btn-outline',
//             buttonup_class: 'btn btn-info btn-outline',
//         });

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
