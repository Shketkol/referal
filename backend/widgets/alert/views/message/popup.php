<? if (!empty($model)) { ?>
    <?
    $type = $model[0]['type'];
    $title = $model[0]['title'];
    $desc = $model[0]['desc'];
    $script = <<< JS
    
    title_popup = '$title', 
    desc_popup = '$desc';  
    swal({
        title: title_popup, 
        text: desc_popup, 
        type: "success",
        timer: 2000,
        });  
    
JS;
    $this->registerJs($script, yii\web\View::POS_END);
    ?>
<? } ?>
