<?php

namespace backend\widgets\alert\assets;


use yii\web\AssetBundle;

class AlertAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $css = [
        'plugins/bower_components/sweetalert/sweetalert.css'
    ];
    public $js = [
        'admin/plugins/bower_components/sweetalert/sweetalert.min.js',
        'admin/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
