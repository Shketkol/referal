<?php
return [
    '/' => 'site/index',

    //auth begin
    'login' => 'auth/users/login',
    'login/error' => 'auth/users/login-error',
    'logout' => 'auth/users/logout',

    'forgot-password' => 'auth/users/forgot',
    'forgot-password/success' => 'auth/users/forgot-success',
    'password-recovery' => 'auth/users/password-recovery',

    'signup' => 'auth/users/signup',
    'signup/success' => 'auth/users/register-success',
    'email' => 'auth/users/email',

    'invite' => 'auth/users/invite',
    //auth end

    //users begin
    '/profile' => 'users/users/profile',
    '/users' => 'users/users/index',
    '/users/create' => 'users/users/create',
    '/users/update' => 'users/users/update',
    '/users/delete' => 'users/users/delete',
    //users end

    'finance' => 'finance/finance/index',
    'finance/create' => 'finance/finance/create',
    'royalty' => 'royalty/royalty/index',
    'income' => 'income/income/index',
    'income/outgo-create' => 'income/income/outgo-create',
    'income/outgo-update' => 'income/income/outgo-update',
    'planning' => 'planning/planning/index',
    'planning/update' => 'planning/planning/update',
    'planning/create' => 'planning/planning/create',

    'product/<user:(.+)>/<product:(.+)>' => 'product/index',

    'outpay/crate' => 'outpay/outpay/create'
];
