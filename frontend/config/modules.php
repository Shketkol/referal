<?php
return [
    'auth' => [
        'class' => 'frontend\modules\auth\Module',
    ],
    'users' => [
        'class' => 'frontend\modules\users\Module',
    ],
    'products' => [
        'class' => 'frontend\modules\products\Module',
    ],
    'leads' => [
        'class' => 'frontend\modules\leads\Module',
    ],
    'bonus' => [
        'class' => 'frontend\modules\bonus\Module',
    ],
    'outpay' => [
        'class' => 'frontend\modules\outpay\Module',
    ],
    'payments' => [
        'class' => 'frontend\modules\payments\Module',
    ],
];