<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AdminMainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
//        'plugins/bower_components/jquery/dist/jquery.min.js',
        'public/bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'public/js/jquery.slimscroll.js',
        'public/js/waves.js',
        'public/js/cbpFWTabs.js',
        'public/js/cbpFWTabs.js',

        'public/js/custom.min.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
        'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',

        'plugins/bower_components/moment/moment.js',
        'plugins/bower_components/timepicker/bootstrap-timepicker.min.js',
        'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js',

        'public/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
