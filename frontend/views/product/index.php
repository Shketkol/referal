<? $form = \yii\widgets\ActiveForm::begin([
    'enableClientValidation' => false,
    'validateOnSubmit' => true,
    'action' => \yii\helpers\Url::to(['/payments/robokassa/index', 'id' => $id, 'payment_method'=>'QCardR']),
]); ?>
<?= $form->errorSummary($client); ?>
<?= $form->field($client, 'name')->textInput(['required' => true]) ?>
<?= $form->field($client, 'email')->textInput(['type' => 'email', 'required' => true]) ?>
<?= $form->field($client, 'phone')->textInput(['required' => true]) ?>
<button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp Pay &nbsp;</button>
<? \yii\widgets\ActiveForm::end(); ?>