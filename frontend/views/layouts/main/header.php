<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="index.html">
                    <span class="hidden-sm hidden-md hidden-lg">
                    </span>
                    <span class="hidden-xs">
                        Баланс: <?=\common\models\Balance::getUser(Yii::$app->user->id)?>
                    </span>
                </a>
            </div>
            <!-- /Logo -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="/plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle hidden"><b class="hidden-xs"><?=Yii::$app->user->identity->name?> <?=Yii::$app->user->identity->usersProfile->surname?></b><span class="caret"></span> </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <div class="dw-user-box p-b-5">
                                <div class="u-text">
                                    <h4><?=Yii::$app->user->identity->name?> <?=Yii::$app->user->identity->usersProfile->surname?></h4>
                                    <p class="text-muted">Ваш ID: <?=Yii::$app->user->identity->id?></p>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=\yii\helpers\Url::to(['/auth/users/logout'])?>"><i class="fa fa-sign-out"></i>&nbsp; Выйти</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>

    <!-- End Top Navigation -->