<?php
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;
/* @var $this yii\web\View */

$this->title = 'Основная информация';
?>
<!--<div class="row">-->
<!--    <div class="col-sm-12">-->
<!--        <div class="white-box">-->
<!--            <div class="row row-in">-->
<!--                <div class="col-lg-3 col-sm-6 row-in-br">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-warning"><i class="fa fa-ruble"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=$all_online?><!--</h3></li>-->
<!--                        <li class="">-->
<!--                            <h4>Сумарная выручка онлайн</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6  row-in-br">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-warning"><i class="fa fa-ruble"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=$all_offline?><!--</h3></li>-->
<!--                        <li class="">-->
<!--                            <h4>Сумарная выручка офлайн</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6  row-in-br">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-warning"><i class="fa fa-align-center"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=$all_lead?><!--</h3></li>-->
<!--                        <li class="">-->
<!--                            <h4>Количество сделок</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6  b-0">-->
<!--                    <ul class="col-in">-->
<!--                        <li>-->
<!--                            <span class="circle circle-md bg-success"><i class="fa fa-sort-numeric-asc"></i></span>-->
<!--                        </li>-->
<!--                        <li class="col-last"><h3 class="counter text-right m-t-15">--><?//=$position?><!--</h3></li>-->
<!--                        <li class="">-->
<!--                            <h4>Позиция в рейтинге</h4>-->
<!--                        </li>-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="col-md-12">-->
<!--        <div class="white-box">-->
<!--            <h1 class="box-title">Последние сделки</h1>-->
<!--            <div class="table-responsive">-->
<!--                --><?php //\yii\widgets\Pjax::begin(); ?>
<!--                --><?//= \yii\grid\GridView::widget([
//                    'dataProvider' => $dataProvider,
//                    'summary' => false,
//                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
//                    'tableOptions' => [
//                        'class' => 'table table-hover manage-u-table'
//                    ],
//                    'columns' => [
//                        [
//                            'attribute' => 'id',
//                            'contentOptions' => ['class' => 'text-center'],
//                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
//                        ],
//                        'name_lead',
//                        'contact',
////                        'state',
//                        'price',
//                        'email',
//                        'phone',
//                        [
//                            'attribute' => 'type',
//                            'value' => function($model){
//                                if($model->type == \common\models\Leads::TYPE_ONLINE){
//                                    return 'онлайн';
//                                }
//                                if($model->type == \common\models\Leads::TYPE_OFFLINE){
//                                    return 'офлайн';
//                                }
//                            },
//                            'filter' => [
//                                \common\models\Leads::TYPE_ONLINE => 'онлайн',
//                                \common\models\Leads::TYPE_OFFLINE => 'офлайн',
//                            ]
//                        ],
//                        'date',
//                    ],
//                ]); ?>
<!---->
<!--                --><?php //\yii\widgets\Pjax::end(); ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
