$('.js_search_income').on('change', function () {
    serch();
})

$('.js_search_outgo').on('change', function () {
    serch1();
})

$('.js_royalty').on('change', function () {
    serch2();
})

function serch() {
    var serch_inp = [];
    $('.js_search_income').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    window.location.search = '?search_income='+inp;
}

function serch1() {
    var serch_inp = [];
    $('.js_search_outgo').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    window.location.search = '?search_outgo='+inp;
}

function serch2() {
    var serch_inp = [];
    $('.js_royalty').each(function (elem, i) {
        $(this).find('option').each(function (elem1, i1) {
            if($(this).is(':selected')){
                serch_inp.push($(this).val());
            }
        })
    });
    var inp = serch_inp.join('-');
    window.location.search = '?search='+inp;
}