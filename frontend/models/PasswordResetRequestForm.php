<?php
namespace frontend\models;

use common\models\Users;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'validateIsset']
        ];
    }

    public function validateIsset()
    {
        $user = Users::findOne([
            'email' => $this->email,
        ]);
        if(!$user || ($user->status != Users::STATUS_ACTIVE && $user->status != Users::STATUS_DEACTIVATED)){
            $this->addError('common', 'Не найден активный пользователь с таким e-mail');
            return false;
        }else {
            return true;
        }
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = Users::findOne([
            'status' => Users::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

//        if (!Users::isPasswordResetTokenValid($user->password_reset_token)) {
//            $user->generatePasswordResetToken();
//            if (!$user->save(false)) {
//                return false;
//            }
//        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}
