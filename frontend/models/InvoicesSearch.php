<?php

namespace frontend\models;

use common\models\Clients;
use common\models\Invoices;
use common\models\Products;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class InvoicesSearch extends Clients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $user_id)
    {
        $query = Invoices::find()
                 ->innerJoinWith(['clients'])
                 ->where(['not', ['clients_id' => null]])
                ->andWhere(['users_id' => $user_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
        ]);

        return $dataProvider;
    }

    public function search_bonus($params, $user_id, $value)
    {
        $query = Invoices::find()
            ->innerJoinWith(['clients'])
            ->where(['not', ['clients_id' => null]])
            ->andWhere(['users_id' => $user_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

            $date = explode(' - ', $value);
            $query->andWhere(['between', 'created_at', $date[0].' 00:00:00', $date[1].' 23:59:59']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
        ]);

        return $dataProvider;
    }
}
