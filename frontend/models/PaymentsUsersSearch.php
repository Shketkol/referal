<?php

namespace frontend\models;

use common\models\Invoices;
use common\models\PaymentsUser;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class PaymentsUsersSearch extends PaymentsUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payments_type', 'status'], 'integer'],
            [['created_at', 'purse_number', 'value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $user_id)
    {
        $query = PaymentsUser::find()
            ->andWhere(['users_id' => $user_id])->orderBy('created_at DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
            'purse_number' => $this->purse_number,
            'payments_type' => $this->payments_type,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
