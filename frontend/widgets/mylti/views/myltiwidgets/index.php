<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Добавить
        </button>
        <div class="clearfix"></div>
    </div>
    <div class="content_field">
        <div class="content-add" style="display: none">
            <div class="item panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title-address"><?= $name ?>:</span>
                    <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                                class="fa fa-minus"></i></button>
                    <br>
                    <? foreach ($fields as $key => $field) { ?>
                        <label for=""><?= $field['label'] ?></label>
                        <?= \yii\helpers\Html::activeInput('text', $model, $attribute . '[][' . $key . ']', ['class' => 'form-control']) ?>
                    <? } ?>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>


        <? if (!empty($model->$attribute)){ ?>
        <? foreach ($model->$attribute as $key => $field) { ?>
            <? $temp = true; ?>
            <? foreach ($field as $k => $value) {
                if (empty($value)) {
                    $temp = false;
                }
            } ?>
            <? if ($temp) { ?>
                <div class="panel-body container-items">
                    <div class="item panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title-address"><?= $name ?>:</span>
                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i
                                        class="fa fa-minus"></i></button>
                            <div class="input">
                                <? foreach ($field as $k => $value) { ?>
                                    <?= $form->field($model, $attribute . '[' . $key . '][' . $k . ']')->textInput(['maxlength' => true, 'value' => $value])->label($fields[$k]['label'])->error(false) ?>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
        <? } ?>
    </div>
    <? } ?>

</div>
</div>


<?php
$this->registerJsFile('@web/source/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>