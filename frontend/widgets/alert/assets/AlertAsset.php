<?php

namespace frontend\widgets\alert\assets;


use yii\web\AssetBundle;

class AlertAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admin/plugins/bower_components/sweetalert/sweetalert.css'
    ];
    public $js = [
        'admin/plugins/bower_components/sweetalert/sweetalert.min.js',
        'admin/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
