<?php

namespace frontend\modules\payments\controllers;

use common\components\lib\Robokassa;
use common\models\Balance;
use common\models\Clients;
use common\models\Invoices;
use common\models\Payments;
use common\models\Products;
use common\models\Users;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Default controller for the `payments` module
 */
class RobokassaController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent:: beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $user = null;
            if (!empty($post['referal_id'])) {
                $user = Users::findOne(['id' => $post['referal_id']]);
                if (empty($user)) {
                    $user = null;
                }
                $user = $user->id;
            }

            $product = Products::findOne(['product_id' => $post['product_id']]);
            if (!empty($product)) {
                $invoice = new Invoices();
                $invoice->users_id = $user;
                $invoice->products_id = $product->id;
                $invoice->price = $product->price;
                $invoice->bonus = $product->bonus;

                $client = new Clients();
                $client->name = $post['client_name'];
                $client->email = $post['client_email'];
                $client->phone = $post['client_phone'];

                $client_find = Clients::findOne(['name' => $client->name, 'email' => $client->email, 'phone' => $client->phone]);

                if (empty($client_find)) {
                    $client->save(false);
                    $client_find = $client;
                }

                $invoice->clients_id = $client_find->id;
                $invoice->amo_id = $post['amo_id'];
                $invoice->robokassa_link = $post['robokassa_link'];
                $invoice->save(false);
            }
        }
    }


    public function actionSuccess()
    {
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $invoice = Invoices::findOne(['amo_id' => $post['amo_id']]);
            if(!empty($invoice)){
                $user = null;
                if(isset($post['referal_id']) && !empty($post['referal_id'])){
                    $user = $post['referal_id'];
                }
                $invoice->users_id = $user;
                $invoice->status = 3;
                $invoice->save();

                if(!is_null($user)){
                    $balance = Balance::findOne(['users_id' => $invoice->users_id]);
                    if (empty($balance)) {
                        $balance = new Balance();
                        $balance->users_id = $invoice->users_id;
                    }
                    $balance->balance = (float)$balance->balance + (float)$invoice->bonus;
                    $balance->save();
                }
            }
        }
    }
}
