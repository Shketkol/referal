<h4 class="box-title">Контактная информация</h4>
<?= $form->field($model, 'content[phones]')->textarea(['required' => true])->label('Контактные телефоны') ?>

<h4 class="box-title">Индивидуальный предприниматель</h4>
<?= $form->field($model, 'content[fio]')->textInput(['required' => true])->label('ФИО') ?>
<?= $form->field($model, 'content[fio_rod]')->textInput(['required' => true])->label('ФИО в родительном падеже') ?>
<?= $form->field($model, 'content[address_yar]')->textarea(['required' => true])->label('Юридический адрес') ?>
<?= $form->field($model, 'content[iin]')->textInput(['required' => true])->label('ИНН') ?>

<h4 class="box-title">Свидетество ОГРНИП</h4>
<?= $form->field($model, 'content[slid_nomer]')->textInput(['required' => true])->label('Номер') ?>
<?= $form->field($model, 'content[slid_date]')->textInput(['required' => true])->label('Дата выдачи') ?>

<h4 class="box-title">Почтовый адрес</h4>
<?= $form->field($model, 'content[address_index]')->textInput(['required' => true])->label('Индекс') ?>
<?= $form->field($model, 'content[address_region]')->textInput(['required' => true])->label('Регион') ?>
<?= $form->field($model, 'content[address_city]')->textInput(['required' => true])->label('Город') ?>
<?= $form->field($model, 'content[address_street]')->textInput(['required' => true])->label('Улица') ?>
<?= $form->field($model, 'content[address_house]')->textInput()->label('Дом') ?>
<?= $form->field($model, 'content[address_flat]')->textInput()->label('Квартира') ?>
<?= $form->field($model, 'content[address_box]')->textInput(['required' => true])->label('Абонентский ящик') ?>

<h4 class="box-title">Паспорт</h4>
<?= $form->field($model, 'content[pasport_series]')->textInput(['required' => true])->label('Серия') ?>
<?= $form->field($model, 'content[pasport_number]')->textInput(['required' => true])->label('Номер') ?>
<?= $form->field($model, 'content[pasport_who]')->textarea(['required' => true])->label('Кем выдан') ?>
<?= $form->field($model, 'content[pasport_date]')->textInput(['required' => true])->label('Дата выдачи') ?>
<?= $form->field($model, 'content[pasport_code]')->textInput(['required' => true])->label('Код подразделени') ?>

<h4 class="box-title">Банк</h4>
<?= $form->field($model, 'content[bank_name]')->textarea(['required' => true])->label('Название') ?>
<?= $form->field($model, 'content[bank_bik]')->textInput(['required' => true])->label('БИК') ?>
<?= $form->field($model, 'content[pasport_number]')->textInput(['required' => true])->label('Расчетный счет') ?>
<?= $form->field($model, 'content[bank_cornumber]')->textInput(['required' => true])->label('Корреспондентский счет') ?>


