<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = Yii::$app->user->identity->name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['profile']];
$this->params['breadcrumbs'][] = (!empty($model->name))?$model->surname.' '.$model->name.' '.$model->patronymic:'Пользователь';
?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'action' => Url::to(['update', 'id' => $model->id]),
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"col-md-12\">{input}<span class=\"help-block hidden\"></span></div>",
                'labelOptions' => ['class' => 'col-md-12'],
            ],
        ]); ?>
        <div class="white-box">
            <div class="row">
                <div class="col-xs-9">
                    <h3 class="box-title m-b-0">Основная информация</h3>
                </div>
            </div>
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true]) ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true]) ?>
                    <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true, 'value' => $model->usersProfile->surname]) ?>
                    <?= $form->field($model, 'patronymic')->textInput(['placeholder' => 'Отчество', 'required' => true, 'value' => $model->usersProfile->patronymic]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true, 'value' => $model->usersProfile->phone]) ?>
                    <?= $form->field($model, 'city')->textInput(['placeholder' => 'Город', 'required' => true, 'value' => $model->usersProfile->city]) ?>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>