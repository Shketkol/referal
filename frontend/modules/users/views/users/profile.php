<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = Yii::$app->user->identity->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-edit m-r-5"></i> Редактировать ', Url::to(['update', 'id' => $model->id]), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="table-responsive">
    <div class="white-box">
        <div class="row">
            <h4 class="box-title">Основная информация</h4>
            <div class="col-md-2 col-xs-6 b-r"><strong>Имя</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->name ?></p>
            </div>
            <div class="col-md-2 col-xs-6 b-r"><strong>Фамилия</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->usersProfile->surname ?></p>
            </div>
            <div class="col-md-2 col-xs-6 b-r"><strong>Отчество</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->usersProfile->patronymic ?></p>
            </div>
            <div class="col-md-2 col-xs-6 b-r"><strong>Email</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->email ?></p>
            </div>
            <div class="col-md-2 col-xs-6 b-r"><strong>Город</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->usersProfile->city ?></p>
            </div>
            <div class="col-md-2 col-xs-6"><strong>Телефон</strong>
                <br>
                <p class="text-muted"><?= Yii::$app->user->identity->usersProfile->phone ?></p>
            </div>
        </div>
    </div>
<!--    <div class="white-box">-->
<!--        <div class="row">-->
<!--            <h4 class="box-title">Дополнительная информация</h4>-->
<!--            --><?//if(!empty($model->usersProfile->content)){?>
<!--            --><?//foreach ($model->usersProfile->content as $key=>$value){?>
<!--                <div class="col-md-12"><strong>--><?//=\common\models\UsersProfile::$types[$model->usersProfile->type]['fields'][$key]?><!--</strong>-->
<!--                    <br>-->
<!--                    <p class="text-muted">--><?//= $value ?><!--</p>-->
<!--                </div>-->
<!--            --><?// }?>
<!--            --><?// }else {?>
<!--                <h4 class="box-title">Заполните профиль</h4>-->
<!--            --><?// }?>
<!--        </div>-->
<!--    </div>-->
</div>
