<?php

namespace frontend\modules\users\controllers;

use frontend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\User;

/**
 * Default controller for the `users` module
 */
class UsersController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfile(){
        $model = $this->loadModel(Yii::$app->user->id);
        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if(!empty($model->usersProfile->type)){
            $model->content = $model->usersProfile->content;
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $userProfile = UsersProfile::findOne(['users_id' => $model->id]);
            $userProfile->surname = $model->surname;
            $userProfile->patronymic = $model->patronymic;
            $userProfile->phone = $model->phone;
            $userProfile->city = $model->city;
            if($userProfile->save()){
                return $this->redirect(['profile']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }


}



