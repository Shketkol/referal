<?
$this->title = 'Выплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('navBlock'); ?>
<?if(!\common\models\Balance::getBalanceUser(Yii::$app->user->id)){?>

    <div id="alerttopright" class="myadmin-alert alert-info myadmin-alert-top-right" style="display: none;"> <a href="#" class="closed">×</a>
        <h4>Вы не можете добавить выплату </h4>У вас в кошельку меньше чем <?=Yii::$app->params['sum']?></div>

    <?
    $script = <<< JS
                   $(document).ready(function() {
                        $("#alerttopright").fadeToggle(350);

                        $(".myadmin-alert .closed").click(function (event) {
                            $(this).parents(".myadmin-alert").fadeToggle(350);
                            return false;
                        });
                        $(".myadmin-alert-click").click(function (event) {
                            $(this).fadeToggle(350);
                            return false;
                        });
                    });
JS;
    $this->registerJs($script, yii\web\View::POS_END);
    ?>
<? }else {?>
    <? echo \yii\helpers\Html::a('<i class="fa fa-plus m-r-5"></i> Добавить выплату', \yii\helpers\Url::to(['/outpay/outpay/create']), [
        'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
    ]) ?>
<? }?>
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'ID',
                            'value' => function ($data) {
                                return $data->id;
                            },
                        ],
                        [
                            'label' => 'Дата выплаты',
                            'value' => function ($data) {
                                return $data->created_at;
                            },
                        ],
                        [
                            'label' => 'Размер выплаты',
                            'value' => function ($data) {
                                return $data->value;
                            },
                        ],
                        [
                            'label' => 'Платежная система',
                            'value' => function ($data) {
                                return \common\models\PaymentsUser::$types[$data->payments_type];
                            },
                        ],
                        [
                            'label' => 'Номер для выплаты',
                            'value' => function ($data) {
                                return $data->purse_number;
                            },
                        ],
                        [
                            'label' => 'Статус ',
                            'value' => function ($data) {
                                if ($data->status == 1) {
                                    return 'в обработке';
                                }
                                if ($data->status == 2) {
                                    return 'выплачен';
                                }
                                if ($data->status == 3) {
                                    return 'не выплачен';
                                }
                            },
                        ]
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

