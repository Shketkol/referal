<?php

namespace frontend\modules\outpay\controllers;

use common\models\Balance;
use common\models\Invoices;
use common\models\PaymentsUser;
use frontend\models\InvoicesSearch;
use frontend\models\PaymentsUsersSearch;
use frontend\models\ProductSearch;
use \frontend\components\controllers\DefaultController;
use Yii;

/**
 * Default controller for the `referal` module
 */
class OutpayController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new PaymentsUser();

        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->users_id = Yii::$app->user->id;
            $model->save();

            $balance = Balance::findOne(['users_id' => Yii::$app->user->id]);
            $balance->balance = (float)$balance->balance - $model->value;
            $balance->save();

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }
}
