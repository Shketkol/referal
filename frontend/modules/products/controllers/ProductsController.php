<?php

namespace frontend\modules\products\controllers;

use frontend\models\ProductSearch;
use \frontend\components\controllers\DefaultController;
use Yii;

/**
 * Default controller for the `referal` module
 */
class ProductsController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
