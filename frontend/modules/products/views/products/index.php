<?
$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="row row-in">
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=\common\models\Invoices::getCountClickLinkAll(Yii::$app->user->id)?></h3></li>
                        <li class="col-middle">
                            <h4>Количество переходов по ссылкам</h4>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=\common\models\Invoices::getCountPay(Yii::$app->user->id)?></h3></li>
                        <li class="col-middle">
                            <h4>Количество продаж</h4>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=\common\models\Invoices::getSumPrice(Yii::$app->user->id)?></h3></li>
                        <li class="col-middle">
                            <h4>Партнерские оплаты</h4>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6  b-0">
                    <ul class="col-in">
                        <li class="col-last"><h3 class="counter text-right m-t-15"><?=\common\models\Invoices::getSumBonus(Yii::$app->user->id)?></h3></li>
                        <li class="col-middle">
                            <h4>Партнерские отчисления</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 80px;']
                        ],
                        'name',
                        'price',
                        'bonus',
                        [
                            'label' => 'Количество переходов по ссылке',
                            'value' => function($data){
                                return \common\models\Invoices::getCountClickLink(Yii::$app->user->id, $data->id);
                            },
                        ],
                        [
                            'label' => 'Реферальная ссылка',
                            'format' => 'raw',
                            'content' => function($data){
                                $user = \common\models\Users::findOne(['id' => Yii::$app->user->id]);
                                $user = \common\models\Users::findOne(['id' => $user->id]);
                                $link = $data->url.'&user='.$user->id;
                                return \yii\helpers\Html::a('Ссылка', $link, ['target' => '_blank']);
                            }
                        ]
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>