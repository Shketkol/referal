<?php

namespace frontend\modules\leads\controllers;

use frontend\models\InvoicesSearch;
use frontend\models\ProductSearch;
use \frontend\components\controllers\DefaultController;
use Yii;

/**
 * Default controller for the `referal` module
 */
class LeadsController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
