<?
$this->title = 'Лиды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <div class="clearfix"></div>

            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'label' => 'Имя',
                            'value' => function($data){
                                return $data->clients->name;
                            },
                        ],
                        [
                            'label' => 'Номер телефона',
                            'value' => function($data){
                                return $data->clients->phone;
                            },
                        ],
                        [
                            'label' => 'Email',
                            'value' => function($data){
                                return $data->clients->email;
                            },
                        ],
                        [
                            'label' => 'Продукт',
                            'value' => function($data){
                                return $data->products->name;
                            },
                        ],
                        'created_at',
                        [
                            'label' => 'Статус ',
                            'value' => function($data){
                                if($data->status == 1 || $data->status == 2){
                                    return 'не оплачен';
                                }
                                if($data->status == 3){
                                    return 'оплачен';
                                }
                            },
                        ]
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>