<?php

namespace frontend\modules\bonus\controllers;

use common\models\Invoices;
use frontend\models\InvoicesSearch;
use frontend\models\ProductSearch;
use \frontend\components\controllers\DefaultController;
use Yii;

/**
 * Default controller for the `referal` module
 */
class BonusController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $time = date('Y-m', time());
        $value = $time.'-1 - '.$time.'-'.date('t', time());
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post('Invoices');
            $value = $post['from_date'];
        }

        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search_bonus(Yii::$app->request->queryParams, Yii::$app->user->id, $value);

        $model = new Invoices();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'value' => $value
        ]);
    }
}
