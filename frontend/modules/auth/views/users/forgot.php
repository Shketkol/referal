<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
?>
<section id="wrapper" class="new-login-register">
    <div class="lg-info-panel">
        <div class="inner-panel">
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Забыли пароль?</h3>
            <small>Пожалуйста, введите ваш электронный адрес ниже. На него вам придет ссылка для восстановления пароля.</small>
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => Url::to(['forgot']),
                    'options' => [
                        'class' => 'form-horizontal form-material new-lg-form',
                        'autocomplete' => 'off',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"col-xs-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    ],
                ]); ?>

                <?=$form->errorSummary($model, ['class'=>'error-summary-no-header']);?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button type="submit"
                                class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Отправить &nbsp;</button>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                        <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p><a href="<?=Url::to(['login'])?>" class="text-primary m-l-5"><b>На страницу входа</b></a></p>
                    </div>
                </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>


</section>

