<?php
use \yii\bootstrap\ActiveForm;
?>
<section id="wrapper" class="new-login-register">
    <div class="lg-info-panel">
        <div class="inner-panel">
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Регистрация в кабинете</h3>
            <small>Пожалуйста, внесите свои данные.</small>
            <? $form = ActiveForm::begin([
                'id' => 'loginform',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
                'action' => \yii\helpers\Url::to(['/auth/users/signup']),
                'options' => [
                    'class' => 'form-horizontal form-material new-lg-form',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-xs-12\">{input}<span class=\"help-block hidden\"></span></div>",
                ],
            ]); ?>
                <?=$form->errorSummary($model);?>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'patronymic')->textInput(['placeholder' => 'Отчество', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'city')->textInput(['placeholder' => 'Город', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'confirm_pass')->textInput(['type' => 'password', 'placeholder' => 'Подтвердите пароль', 'required' => true])->label(false) ?>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Регистрация &nbsp;</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Уже зарегистрированы? <a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>" class="text-primary m-l-5"><b>Войти в аккаунт</b></a></p>
                    </div>
                </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</section>

<script>
    var password = document.getElementById("users-password_hash")
        , confirm_password = document.getElementById("users-confirm_pass");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Пароли не совпадают");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

<style>
    .new-login-register .new-login-box {
        margin-top: 5%;
    }
</style>