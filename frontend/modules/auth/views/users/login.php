<?php
use \yii\bootstrap\ActiveForm;
?>
<section id="wrapper" class="new-login-register">
    <div class="lg-info-panel">
        <div class="inner-panel">
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Войти в кабинет</h3>
            <small>Введите Ваш e-mail и пароль</small>
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => \yii\helpers\Url::to(['/auth/users/login']),
                    'options' => [
                        'class' => 'form-horizontal form-material new-lg-form',
                        'autocomplete' => 'off',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"col-xs-12\">{input}<span class=\"help-block hidden\"></span></div>",
                    ],
                ]); ?>

                <?=$form->errorSummary($model, ['header'=>'<p>Ошибки при авторизации:</p>']);?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'password')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-info pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox" name="LoginForm[rememberMe]" value="1">
                            <label for="checkbox-signup"> Запомнить меня </label>
                        </div>
                        <a href="<?=\yii\helpers\Url::to(['/auth/users/forgot'])?>" class="text-dark pull-right"><i
                                class="fa fa-lock m-r-5"></i> Забыли пароль?</a></div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Войти &nbsp;</button>
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                        <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Нет учетной записи? <a href="<?=\yii\helpers\Url::to(['/auth/users/signup'])?>" class="text-primary m-l-5"><b>Зарегистрируйтесь</b></a></p>
                    </div>
                </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>


</section>
