<section id="wrapper" class="new-login-register">
    <div class="lg-info-panel">
        <div class="inner-panel">
        </div>
    </div>
    <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title">Восстановление пароля</h3>
            <p>Инструкции по восстановлению пароля отправлены на указанный адрес электронной почты.</p>

            <p class="text-center m-t-20"><a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>" class="btn btn-info waves-effect waves-light"><b>На страницу входа</b></a></p>
        </div>
    </div>
</section>