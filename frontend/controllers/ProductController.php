<?php
namespace frontend\controllers;

use common\models\Clients;
use common\models\Invoices;
use common\models\Products;
use common\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class ProductController extends Controller
{

    /**
     * Пример для отправки информации о сделке и попытке покупки
     */
    public function actionIndex()
    {
        /*
         * client_name - Имя покупателя
         * client_email - Почта покупателя
         * client_phone - Телефон покупателя
         * referal_id - ID реферала (может быть null)
         * product_id - ID продукта
         * amo_id - ID сделки в AmoCRM
         * robokassa_link - Ссылка на оплату в робокасе
         */
        $params = array(
            'client_name' => 'Nick56',
            'client_email' => 'test@gmail.com',
            'client_phone' => '0963305869',
            'referal_id' => 6,
            'product_id' => 1515,
            'amo_id' => 44153736,
            'robokassa_link' => 'http://referal.new.local/dev/db/?username=root&db=referal&select=clients&where%5B0%5D%5Bcol%5D=id&where%5B0%5D%5Bop%5D=%3D&where%5B0%5D%5Bval%5D=5'
        );

        $ch = curl_init();
        //понетять   http://{название сервера}/payments/robokassa/index
        curl_setopt($ch, CURLOPT_URL, 'http://referal.new.local/payments/robokassa/index');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Пример для отправки информации покупки продукта
     */
    public function actionResult()
    {
        /*
         * referal_id - ID реферала (может быть null)
         * amo_id - ID сделки в AmoCRM
         */
        $params = array(
            'referal_id' => 6,
            'amo_id' => 44153736,
        );

        $ch = curl_init();
        //понетять   http://{название сервера}/payments/robokassa/success
        curl_setopt($ch, CURLOPT_URL, 'http://referal.new.local/payments/robokassa/success');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($ch);
        curl_close($ch);
    }


}
