<?php

namespace common\models;

use common\components\traits\Validation;
use Yii;

/**
 * This is the model class for table "users_profile".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $surname
 * @property string $patronymic
 * @property string $post
 *
 * @property Users $users
 */
class UsersProfile extends \yii\db\ActiveRecord
{
    use Validation;

    const TYPE_IP = 1;
    const TYPE_OOO = 2;

    public static $types = [
        self::TYPE_IP => array(
            'customForm' => '_ip',
            'fields' => array(
                'phones' => 'Контактные телефоны',
                'fio' => 'ФИО',
                'fio_rod' => 'ФИО в родительном падеже',
                'address_yar' => 'Юридический адрес',
                'iin' => 'ИНН',
                'slid_nomer' => 'Номер ОГРНИП',
                'slid_date' => 'Дата выдачи ОГРНИП',
                'address_index' => 'Индекс',
                'address_region' => 'Регион',
                'address_city' => 'Город',
                'address_street' => 'Улица',
                'address_house' => 'Дом',
                'address_flat' => 'Квартира',
                'address_box' => 'Абонентский ящик',
                'pasport_series' => 'Паспорт Серия',
                'pasport_number' => 'Паспорт Номер',
                'pasport_who' => 'Паспорт Кем выдан',
                'pasport_date' => 'Паспорт Дата выдачи',
                'pasport_code' => 'Паспорт Код подразделени',
                'bank_name' => 'Банк Название',
                'bank_bik' => 'Банк БИК',
                'pasport_number' => 'Банк Расчетный счет',
                'bank_cornumber' => 'Корреспондентский счет',
            )
        ),
        self::TYPE_OOO => array(
            'customForm' => '_ooo'
        ),
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'type'], 'integer'],
            [['surname', 'patronymic', 'city', 'phone'], 'string', 'max' => 255],
            [['content'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'city' => 'Город',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @inheritdoc
     * @return UsersProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersProfileQuery(get_called_class());
    }

    public function getArrayField(){
        return array('content');
    }
}
