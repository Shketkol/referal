<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "balance".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $balance
 *
 * @property Users $users
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id'], 'required'],
            [['users_id'], 'integer'],
            [['balance'], 'number'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Users ID',
            'balance' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }


    public static function getBalanceUser($user)
    {
        $sum = Yii::$app->params['sum'];
        $out = PaymentsUser::findOne(['users_id' => $user]);
        if (empty($out)) {
            $balance = Balance::findOne(['users_id' => $user]);
            if (!empty($balance) && $balance->balance >= $sum) {
                return true;
            }else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function getUser($user)
    {
        $balance = Balance::findOne(['users_id' => $user]);
        return (!empty($balance)) ? (int)$balance->balance : 0;
    }
}
