<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $products_id
 * @property string $price
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $users
 * @property Products $products
 * @property Payments[] $payments
 */
class Invoices extends \yii\db\ActiveRecord
{
    public $from_date;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['products_id', 'price'], 'required'],
            [['users_id', 'products_id', 'status', 'clients_id'], 'integer'],
            [['price', 'bonus'], 'number'],
            [['amo_id'], 'string'],
            [['created_at', 'updated_at', 'robokassa_link'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['products_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['products_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Users ID',
            'products_id' => 'Products ID',
            'price' => 'Price',
            'status' => 'Status',
            'created_at' => 'Дата регистрации',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'products_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['invoices_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasOne(Clients::className(), ['id' => 'clients_id']);
    }

    public static function getCountClickLink($user_id, $product_id){
        $count = Invoices::find()
            ->where(['users_id' => $user_id, 'products_id' => $product_id])
            ->count();
        return $count;
    }

    public static function getCountClickLinkAll($user_id){
        $count = Invoices::find()
            ->where(['users_id' => $user_id])
            ->count();
        return $count;
    }

    public static function getCountPay($user_id){
        $count = Invoices::find()
            ->where(['users_id' => $user_id])
            ->andWhere(['status' => 3])
            ->count();
        return $count;
    }

    public static function getSumPrice($user_id){
        $count = Invoices::find()
            ->select('SUM(price) as price')
            ->where(['users_id' => $user_id])
            ->andWhere(['status' => 3])
            ->asArray()
            ->one();
        return (!empty($count['price'])) ? $count['price'] : '0.00';
    }

    public static function getSumBonus($user_id){
        $count = Invoices::find()
            ->select('SUM(bonus) as bonus')
            ->where(['users_id' => $user_id])
            ->andWhere(['status' => 3])
            ->asArray()
            ->one();
        return (!empty($count['bonus'])) ? $count['bonus'] : '0.00';
    }

    public static function getSumBonusPeriod($user_id, $period = null){
        if($period == null){
            $count = Invoices::find()
                ->select('SUM(bonus) as bonus')
                ->where(['users_id' => $user_id])
                ->andWhere(['status' => 3])
                ->asArray()
                ->one();
            return (!empty($count['bonus'])) ? $count['bonus'] : '0.00';
        }else {
            $date = explode(' - ', $period);
            $count = Invoices::find()
                ->select('SUM(bonus) as bonus')
                ->where(['users_id' => $user_id])
                ->andWhere(['status' => 3])
                ->andWhere(['between', 'created_at', $date[0].' 00:00:00', $date[1].' 23:59:59'])
                ->asArray()
                ->one();
            return (!empty($count['bonus'])) ? $count['bonus'] : '0.00';
        }

    }
}
