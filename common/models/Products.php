<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $name
 *
 * @property Users $users
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'token', 'bonus', 'price', 'product_id', 'url'], 'required'],
            [['name', 'token', 'url', 'product_id'], 'string', 'max' => 300],
            [['bonus', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'bonus' => 'Вознаграждение',
            'price' => 'Цена',
            'url' => 'Ссылка',
            'product_id' => 'ID продукта на сайте LikeCentre'
        ];
    }
    /**
     * @inheritdoc
     * @return ProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductsQuery(get_called_class());
    }
}
