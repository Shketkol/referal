<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "payments_user".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $value
 * @property integer $payments_type
 * @property string $purse_number
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $users
 */
class PaymentsUser extends \yii\db\ActiveRecord
{
    const TYPE_YANDEX = 1;
    const TYPE_ROBOKASSA = 2;

    public static $types = array(
        self::TYPE_YANDEX => 'Яндекс деньги',
        self::TYPE_ROBOKASSA => 'ROBOKASSA',
    );


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'payments_type', 'purse_number'], 'required'],
            [['users_id', 'payments_type', 'status'], 'integer'],
            [['value'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['purse_number'], 'string', 'max' => 300],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            ['value', 'validateValue']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'value' => 'Сумма',
            'payments_type' => 'Тип платежной системы',
            'purse_number' => 'Номер кошелька/карточки/счета',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    public function validateValue()
    {
        $balance = Balance::findOne(['users_id' => Yii::$app->user->id]);
        if (empty($balance)) {
            $this->addError('value', 'У вас пустой кошелек');
            return false;
        }
        if ($this->value > $balance->balance){
            $this->addError('value', 'Нехватает денег в кошельке');
            return false;
        }
        return true;
    }
}
