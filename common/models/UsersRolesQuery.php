<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UsersRoles]].
 *
 * @see UsersRoles
 */
class UsersRolesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UsersRoles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UsersRoles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
