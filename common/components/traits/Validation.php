<?php
/**
 * Created by PhpStorm.
 * User: metal
 * Date: 11.10.15
 * Time: 22:19
 */

namespace common\components\traits;

/**
 * Class IsPublishedTrait
 */
trait Validation
{
    /**
     * @return $this
     */
    public function beforeSave($insert)
    {
        if (method_exists($this, 'getArrayField')) {
            $attribute_array = $this->getArrayField();
            foreach ($attribute_array as $attr) {
                $array = array();
                if (count($this->$attr) >= 1) {
                    if (is_array($this->$attr)) {
                        foreach ($this->$attr as $key => $value) {
                            if (is_array($value)) {
                                foreach ($value as $k => $val) {
                                    if (!empty($value[$k])) {
                                        $array[$key][$k] = $val;
                                    }
                                }
                                if (!empty($array)) {
                                    $this->$attr = json_encode(array_values($array));
                                } else {
                                    $this->$attr = null;
                                }
                            } else {
                                //$this->$attr = json_encode($this->$attr);
                                if (!empty($value)) {
                                    $array[$key] = $value;
                                    if (!empty($array)) {
                                        $this->$attr = json_encode($array);
                                    } else {
                                        $this->$attr = null;
                                    }
                                }else {
                                    if (!empty($array)) {
                                        $this->$attr = json_encode($array);
                                    } else {
                                        $this->$attr = null;
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        if (method_exists($this, 'getPositionField')) {
            $model = $this::className();
            if (empty($this->id)) {
                $field = $this->getPositionField();
                $this->$field = 1;
                $models = $model::find()
                    ->where(['type' => $this->type])
                    ->all();
                foreach ($models as $model) {
                    $model->$field = (int)$model->$field + 1;
                    $model->save(false);
                }
            }
        }


        return parent::beforeSave($insert);
    }


    public function afterFind()
    {
        if (method_exists($this, 'getArrayField')) {
            $attribute_array = $this->getArrayField();
            foreach ($attribute_array as $attribute) {
                if (count($this->$attribute) > 0) {
                    $this->$attribute = (array)json_decode($this->$attribute);
                };
            };
        }

    }


}